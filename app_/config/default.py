import typing as tp

from pydantic import BaseSettings, Field


class DefaultSettings(BaseSettings):
    """
    Default configs for application.
    """

    ENV: str = Field('default', env='ENV')
    PATH_PREFIX: str = Field('/', env='PATH_PREFIX')
    APP_HOST: str = Field('http://127.0.0.1', env='APP_HOST')
    APP_PORT: int = Field(5000, env='APP_PORT')

    POSTGRES_DB: str = Field('test', env='POSTGRES_DB')
    POSTGRES_HOST: str = Field('test_host', env='POSTGRES_HOST')
    POSTGRES_USER: str = Field('test_user', env='POSTGRES_USER')
    POSTGRES_PORT: int = Field('5432', env='POSTGRES_PORT')
    POSTGRES_PASSWORD: str = Field('hackme', env='POSTGRES_PASSWORD')
    DB_CONNECT_RETRY: int = Field(20, env='DB_CONNECT_RETRY')
    DB_POOL_SIZE: int = Field(15, env='DB_POOL_SIZE')

    APP_SECRET_KEY: str = Field('secret_key', env='APP_SECRET_KEY')
    UPLOAD_DIR: str = Field('upload', env='UPLOAD_DIR')
    TMP_DIR: str = Field('tmp', env='TMP_DIR')
    FROM_NAME: str = Field('Test', env='FROM_NAME')
    EMAIL_ADDR: str = Field('email_addr', env='EMAIL_ADDR')
    SMTP_ADDR: str = Field('smtp_addr', env='SMTP_ADDR')
    SMTP_PASS: str = Field('smtp_pass', env='SMTP_PASS')

    BOT_HELPER_TOKEN: str = Field('BOT_HELPER_TOKEN', env='BOT_HELPER_TOKEN')
    BOT_CHAT_ID: str = Field("132", env='BOT_CHAT_ID')

    CONTAINER_CREATE_TIMEOUT: float = Field('600', env='CONTAINER_CREATE_TIMEOUT')
    CONTAINER_STARTUP_BUDGET: float = Field('60', env='CONTAINER_STARTUP_BUDGET')
    CONTAINER_WARMUP_ATTEMPTS: int = Field('3', env='CONTAINER_WARMUP_ATTEMPTS')

    @property
    def database_settings(self) -> tp.Dict[str, tp.Union[str, int]]:
        """
        Get all settings for connection with database.
        """
        return {
            'database': self.POSTGRES_DB,
            'user': self.POSTGRES_USER,
            'password': self.POSTGRES_PASSWORD,
            'host': self.POSTGRES_HOST,
            'port': self.POSTGRES_PORT,
        }

    @property
    def database_uri_async(self) -> str:
        """
        Get uri for connection with database.
        """
        return 'postgresql+asyncpg://{user}:{password}@{host}:{port}/{database}'.format(
            **self.database_settings,
        )

    @property
    def database_uri_sync(self) -> str:
        """
        Get uri for connection with database.
        """
        return (
            'postgresql+psycopg2://{user}:{password}@{host}:{port}/{database}'.format(
                **self.database_settings,
            )
        )

    class Config:
        env_file = '~/.env'
        env_file_encoding = 'utf-8'
