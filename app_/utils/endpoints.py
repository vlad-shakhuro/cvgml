from flask import Flask

from app_.config import DefaultSettings
from app_.endpoints import list_of_routes


def bind_routes(application: Flask, settings: DefaultSettings) -> None:
    """
    Bind all routes to application.
    """
    for route in list_of_routes:
        application.register_blueprint(route, url_prefix=settings.PATH_PREFIX)
