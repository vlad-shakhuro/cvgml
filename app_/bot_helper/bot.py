import asyncio
import html
import logging
import traceback
from datetime import datetime

from aiogram import Bot
from aiogram.dispatcher.webhook import SendMessage

from app_.config import get_settings


logging.basicConfig(level=logging.INFO)
settings = get_settings()
bot = Bot(token=settings.BOT_HELPER_TOKEN)


async def _send_error_to_char(error_text):
    sender = SendMessage(
        chat_id=int(settings.BOT_CHAT_ID),
        text=error_text,
        parse_mode='HTML',
    )
    await sender.execute_response(bot)
    sender.cleanup()


def send_error(text):
    asyncio.run(_send_error_to_char(text))


def send_exception(location, status):
    error = str(traceback.format_exc())
    error = html.escape(error)
    now = datetime.now().strftime('%H:%M:%S %d-%m-%Y')
    error_text = f"<b>CRIT</b> on <b>{location}</b>\n" \
                 f"at <b><i>{now}</i></b>\n" \
                 f"<i>{status}</i>\n\n"

    remaining = 4000 - len(error_text) - len("<code>(truncated)\n</code>")
    if len(error) > remaining:
        error = "(truncated)\n" + error[len(error) - remaining:]
    error_text += f"<code>{error}</code>"

    send_error(error_text)
