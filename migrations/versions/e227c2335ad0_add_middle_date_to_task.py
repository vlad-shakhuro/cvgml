"""Add middle date to task

Revision ID: e227c2335ad0
Revises: 8c18af438e56
Create Date: 2019-12-13 18:51:43.991775

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'e227c2335ad0'
down_revision = '8c18af438e56'
branch_labels = None
depends_on = None


def upgrade():
    op.add_column('attached_task', sa.Column('middle_date', sa.DateTime(), nullable=True))


def downgrade():
    op.drop_column('attached_task', 'middle_date')
