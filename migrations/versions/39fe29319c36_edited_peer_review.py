"""Move peer review to the task table

Revision ID: 39fe29319c36
Revises: 8086af616764
Create Date: 2022-07-29 22:27:33.747573

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import postgresql
from sqlalchemy.sql import table

# revision identifiers, used by Alembic.
revision = '39fe29319c36'
down_revision = '8086af616764'
branch_labels = None
depends_on = None


def upgrade():
    op.add_column('task', sa.Column('use_peer_review', postgresql.BOOLEAN, nullable=True, default=False))
    op.add_column('task', sa.Column('peer_review_criteria', postgresql.JSON, nullable=True, default=None))
    op.add_column('task', sa.Column('peer_review_items', postgresql.JSON, nullable=True, default=None))

    conn = op.get_bind()
    conn.execute('''
    UPDATE task
    SET use_peer_review = (SELECT use_peer_review FROM attached_task WHERE task_id = task.id ORDER BY id DESC LIMIT 1),
        peer_review_criteria = (SELECT peer_review_criteria FROM attached_task WHERE task_id = task.id ORDER BY id DESC LIMIT 1),
        peer_review_items = (SELECT peer_review_items FROM attached_task WHERE task_id = task.id ORDER BY id DESC LIMIT 1)
    ''')


def downgrade():
    op.drop_column('task', 'peer_review_items')
    op.drop_column('task', 'peer_review_criteria')
    op.drop_column('task', 'use_peer_review')
