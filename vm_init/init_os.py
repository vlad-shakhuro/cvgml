#!/usr/bin/env python3

from fabric import Connection
from patchwork.transfers import rsync
from sys import exit

def install_packages(c, *pkg_names):
    c.run('sudo apt-get update', hide=True)
    for pkg_name in pkg_names:
        c.run(f'sudo apt-get install -y {pkg_name}', hide=True)
        print(f'Installed {pkg_name}')

def install_docker(c):
    cmds = ['sudo apt-get install -y apt-transport-https ca-certificates curl software-properties-common',
            'curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -',
            'sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu focal stable"',
            'sudo apt-get update',
            'sudo apt-get install -y docker-ce',
            'sudo usermod -aG docker ${USER}']
    for cmd in cmds:
        c.run(cmd, hide=True)
    print('Installed docker')

def create_db(c):
    c.run('sudo -u postgres psql -c "CREATE DATABASE main"', hide=True)
    c.run('sudo sed -i  "/^host    all             all             127.0.0.1\/32            md5/ s/md5/trust/" /etc/postgresql/12/main/pg_hba.conf')
    c.run('sudo service postgresql restart')
    print('Created db main')

def copy_code(c):
    for code_dir in ['cvgml', 'cvgml-dev']:
        rsync(c, '..', f'/home/cvgml/{code_dir}', exclude=('.git', '__pycache__'), rsync_opts='-q')
    print('Copied code from current repo')

def create_venv(c):
    cmds = ['mkdir -p ~/venv',
            'python3 -m venv ~/venv/flask-deploy',
            'source ~/venv/flask-deploy/bin/activate; pip install --upgrade pip; pip install poetry; poetry install']
    for cmd in cmds:
        c.run(cmd, hide=True)
    print('Created venv')

def init_db(c):
    c.run('source ~/venv/flask-deploy/bin/activate; cd ~/cvgml; ./app.py clean; flask db stamp head', hide=True)
    print('Initialized db')

def set_config(c):
    for src, dst in [('~/cvgml/vm_init/supervisor/*.conf', '/etc/supervisor/conf.d'),
                     ('~/cvgml/vm_init/nginx/nginx.conf', '/etc/nginx'),
                     ('~/cvgml/vm_init/nginx/cvgml*', '/etc/nginx/sites-available')]:
        c.run(f'sudo cp {src} {dst}')

    c.run('mkdir ~/logs')
    c.run('sudo service supervisor restart')

    c.run('sudo mkdir /var/log/cvgml; sudo chown root:adm /var/log/cvgml')
    c.run('sudo rm /etc/nginx/sites-enabled/default')
    c.run('cd /etc/nginx/sites-enabled; sudo ln -s ../sites-available/cvgml cvgml; sudo ln -s ../sites-available/cvgml-dev cvgml-dev')
    c.run('sudo nginx -t', hide=True)
    c.run('sudo service nginx reload')
    print('Configured services')

def set_timezone(c):
    c.run('sudo timedatectl set-timezone Europe/Moscow', hide=True)

def install_certbot(c):
    install_packages(c, 'snapd')
    c.run('sudo snap install core; sudo snap refresh core', hide=True)
    c.run('sudo snap install --classic certbot', hide=True)
    c.run('sudo ln -s /snap/bin/certbot /usr/bin/certbot')
    c.run('sudo certbot --nginx --non-interactive --agree-tos -m services@graphics.cs.msu.ru -d cv-gml.ru -d dev.cv-gml.ru', hide=True)
    print('Obtained certificate for cv-gml.ru and dev.cv-gml.ru')


c = Connection('cvgml')

install_packages(c, 'nginx', 'postgresql', 'redis', 'supervisor', 'python3-venv')
install_docker(c)
copy_code(c)
create_venv(c)
create_db(c)
init_db(c)
set_config(c)
set_timezone(c)
install_certbot(c)
