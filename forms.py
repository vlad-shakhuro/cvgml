from flask_wtf import FlaskForm
from flask_wtf.file import FileSize
from variant_form import VariantFormField, DecimalFieldRu
from wtforms import (
    BooleanField,
    DateTimeField,
    Field,
    FieldList,
    FileField,
    Form,
    FormField,
    PasswordField,
    RadioField,
    SelectField,
    SelectMultipleField,
    StringField,
    TextAreaField,
)
from wtforms.fields.html5 import IntegerField
from wtforms.validators import (
    Email,
    InputRequired,
    Length,
    NumberRange,
    Optional,
    Regexp,
    ValidationError,
)
from app_.config import get_settings
from differences import RESULT_REGEXP_REGISTER_FORM

from flask_wtf.file import FileSize

MB = 1024 ** 2
APP_SETTINGS = get_settings()


class MdTextField(Field):
    pass


class LoginForm(FlaskForm):
    email = StringField('Email')
    password = PasswordField('Пароль')


class RestorePassForm(FlaskForm):
    email = StringField('Email')


# def unique_email(form, field):
#    if User.query.filter_by(email=field.data).first():
#        raise ValidationError('пользователь с такой почтой уже существует')

class RegisterForm(FlaskForm):
    email = StringField('Email',
                        RESULT_REGEXP_REGISTER_FORM[APP_SETTINGS.APP_HOST],
                        description='используется для входа в систему и оповещений по курсу')
    password = PasswordField('Пароль',
                             [Regexp('^[a-zA-Z0-9]{8,}$', message='латиницей и цифрами, 8 и более символов')],
                             description='латиницей и цифрами, 8 и более символов')
    lastname = StringField('Фамилия',
                           [Regexp('^[\u0401\u0410-\u044f\u0451]{1,}(-[\u0401\u0410-\u044f\u0451]{1,})?$',
                                   message='кириллицей')],
                           description='кириллицей')
    firstname = StringField('Имя',
                            [Regexp('^[\u0401\u0410-\u044f\u0451]{3,}$', message='кириллицей')],
                            description='кириллицей')
    patronym = StringField('Отчество',
                           [Regexp('^[\u0401\u0410-\u044f\u0451]{1,}([- ][\u0401\u0410-\u044f\u0451]{1,})?$',
                                   message='кириллицей'), Optional()],
                           description='кириллицей')


class CourseForm(FlaskForm):
    name = StringField('Название курса', [InputRequired('название не может быть пустым')])
    short_name = StringField('Короткое имя', [Regexp('^[a-z][a-z0-9-]{4,}$',
                                                     message='латиница, цифры и дефисы, от 5 символов')],
                             description='латиница, цифры и дефисы, от 5 символов')
    info = TextAreaField('Материалы курса', render_kw={'style': 'font-family: monospace'},
                         description='произвольный текст, поддерживается markdown, html и tex')
    user_info_name = StringField('Запрашиваемая при регистрации на курс информация',
                                 description='например, номер группы')
    user_info_help = StringField('Подсказка по формату информации', description='например, три цифры')
    user_info_regex = StringField('Регулярное выражение для проверки', description='например, ^[0-9]{3}$')
    codeword_end_date = DateTimeField('Срок окончания действия кодового слова', format='%d.%m.%Y %H:%M',
                                      validators=[Optional()])


class GroupForm(FlaskForm):
    name = StringField('Название группы', [InputRequired('название не может быть пустым')])
    codeword_end_date = DateTimeField('Срок окончания действия кодового слова', format='%d.%m.%Y %H:%M',
                                      validators=[Optional()])


class RoleForm(FlaskForm):
    user_id = SelectField('Студент', coerce=int)
    role = SelectField('Роль',
                       choices=[('teacher', 'Преподаватель'),
                                ('assistant', 'Ассистент')])


class SolutionFile(Form):
    name = StringField('Имя файла', [InputRequired('имя файла не может быть пустым')],
                       render_kw={'class': 'form-control'})
    max_size = IntegerField('Макс. размер в Мб', render_kw={'class': 'form-control'})


class ProgrammingTaskForm(FlaskForm):
    name = StringField('Название задания', [InputRequired('название задания не может быть пустым')])
    intro_description = TextAreaField(
        'Вступление',
        description='здесь можно написать необходимые общие слова и '
                    'обозначения, полезные при выполнении задания на программирование,'
                    ' поддерживается markdown, html и tex'
    )
    time_limit = IntegerField('Лимит времени на тест в секундах',
                              [NumberRange(1, 3600, message='лимит времени на тест от 1 до 3600 секунд')],
                              default=10, render_kw={'min': '1', 'max': '3600', 'step': '1'})
    memory_limit = IntegerField('Лимит памяти на тест в Мб',
                                [NumberRange(1, 4096, message='лимит памяти на тест от 1 до 4096 Мб')],
                                default=512, render_kw={'min': '1', 'max': '4096', 'step': '1'})
    img_id = SelectField('Образ docker', coerce=int)
    solutions_per_day = IntegerField('Максимальное число посылок в день',
                                     [NumberRange(1, 20, message='число посылок от 1 до 20')],
                                     default=3, render_kw={'min': '1', 'max': '20', 'step': '1'})
    solution_files = FieldList(FormField(SolutionFile, 'Файлы задания'),
                               min_entries=1)
    task_description = FileField('Описание задания')
    solution_template = FileField('ZIP-архив с шаблоном решения')
    public_tests = FileField('ZIP-архив с публичными тестами',
                             description='в корне архива должны находиться папки вида %02d_(test|unittest)_name_input с тестами и %02d_(test|unittest)_name_gt с ответами')
    private_tests = FileField('ZIP-архив с приватными тестами')
    additional_files = FileField('ZIP-архив с дополнительным файлами',
                                 description='в архиве должны находиться файлы, которые будут добавлены к решению студента при проверке')
    pre_script = FileField('Скрипт для предобработки (компиляция, проверка стиля)')
    run_script = FileField('Скрипт для запуска программы на тестах и выставления оценки')
    post_script = FileField('Скрипт для постобработки (визуализация результатов тестов)')

    default_filenames = {
        'task_description': 'description.pdf',
        'solution_template': 'solution_template.zip',
        'public_tests': 'public_tests.zip',
        'private_tests': 'private_tests.zip',
        'additional_files': 'additonal_files.zip',
        'pre_script': 'pre.py',
        'run_script': 'run.py',
        'post_script': 'post.py'
    }


class ReviewRule(Form):
    mark = DecimalFieldRu('Балл',
                          [NumberRange(0, 10, message='балл от 0 до 10')],
                          default=0, places=2, render_kw={'class': 'form-control', 'step': 0.01, 'min': 0, 'max': 50})
    mark_description = StringField('Критерий', render_kw={'class': 'form-control'})


class ReviewItem(Form):
    name = StringField('Название пункта', render_kw={'class': 'form-control'}, validators=[Optional()])
    rules = FieldList(FormField(ReviewRule, 'Критерии'), min_entries=2)


class NotebookTaskForm(FlaskForm):
    name = StringField('Название задания', [InputRequired('название задания не может быть пустым')])
    intro_description = TextAreaField(
        'Вступление',
        description='здесь можно написать необходимые общие слова и '
                    'обозначения, полезные при выполнении задания на программирование,'
                    ' поддерживается markdown, html и tex'
    )
    solution_files = FieldList(FormField(SolutionFile, 'Файлы задания'), min_entries=1)
    solution_template = FileField('ZIP-архив с шаблоном решения')
    solution_reference = FileField('ZIP-архив с выполненным ноутбуком',
                                   description='решение увидят только ассистенты и преподаватели курса')

    default_filenames = {
        'solution_template': 'solution_template.zip',
        'solution_reference': 'solution_reference.zip',
    }

    use_peer_review = BooleanField('Использовать кросс-проверку')
    peer_review_criteria = TextAreaField('Описание критериев оценки при кросс-проверке',
                                         description='произвольный текст, поддерживается markdown, html и tex')
    peer_review_items = FieldList(FormField(ReviewItem, 'Критерии оценки'), min_entries=1)


class TestForm(Form):
    class AnsForm(Form):
        right = BooleanField("")
        ans = StringField("", validators=[InputRequired()])

    answers = FieldList(FormField(AnsForm, 'Ответы'), min_entries=1)


class NumberForm(Form):
    ans = DecimalFieldRu('Ответ')
    eps = DecimalFieldRu('Погрешность', default=0, places=3, description='для целых чисел 0, для вещественных 0.001')


class QuestionForm(Form):
    question = TextAreaField('Текст вопроса',
                             validators=[InputRequired('текст вопроса не должен быть пустым')],
                             description='произвольный текст, поддерживается markdown, html и tex',
                             render_kw={'rows': '8'})
    img = FileField('Изображение', description='png, ширина 400 пикселей')
    mark = DecimalFieldRu('Оценка',
                          [NumberRange(0, 10, message='оценка от 0 до 10')],
                          default=1, places=2, render_kw={'class': 'form-control', 'step': 0.01, 'min': 0, 'max': 50})
    question_type = SelectField('Вид вопроса',
                                choices=[('test', 'Тест'),
                                         ('number', 'Число')], default='number', render_kw={'data-variant-choose': ''})
    answers = VariantFormField({
        'test': TestForm,
        'number': NumberForm
    }, 'number')
    ans_explanation = TextAreaField('Объяснение правильного ответа',
                                    description='произвольный текст, поддерживается markdown, html и tex')


class TestInfo(Form):
    intro_description = TextAreaField('Вступление',
                                      description='здесь можно написать необходимые общие слова и обозначения, полезные при выполнении теста, поддерживается markdown, html и tex')
    questions = FieldList(FormField(QuestionForm, 'Вопросы'), min_entries=1)
    random_order = BooleanField('Показывать вопросы в случайном порядке')
    random_answer_order = BooleanField('Показывать ответы на тестовые вопросы в случайном порядке')
    sample_questions = BooleanField('Выбирать случайный набор вопросов')
    many_attempts = BooleanField('Сдавать тест несколько раз')
    n_questions = IntegerField('Количество выбираемых вопросов', [Optional()])


class TaskForm(FlaskForm):
    name = StringField('Название задания',
                       [InputRequired('название задания не может быть пустым')])
    info = FormField(TestInfo)


class AttachTaskForm(FlaskForm):
    group_id = SelectField('Группа', coerce=int, validators=[InputRequired()])
    task_id = SelectField('Задание', coerce=int)

    start_date = DateTimeField('Старт', format='%d.%m.%Y %H:%M')
    middle_date = DateTimeField('Промежуточный финиш', format='%d.%m.%Y %H:%M', validators=[Optional()])
    finish_date = DateTimeField('Финиш', format='%d.%m.%Y %H:%M')
    mark_formula = StringField('Формула оценки', default='{best_before_finish}',
                               description='доступные переменные: best_before_middle, best_before_finish, last_before_finish, best_after_finish')
    mark_formula_description = StringField('Описание формулы оценки',
                                           validators=[Length(max=1000)])
    hide_results_before_deadline = BooleanField(
        'Скрывать результаты до дедлайна', default='checked')
    allow_solutions_after_deadline = BooleanField('Разрешить посылки после дедлайна')

    use_peer_review = BooleanField('Использовать кросс-проверку')
    peer_review_date = DateTimeField('Срок кросс-проверки', format='%d.%m.%Y %H:%M', validators=[Optional()])
    min_peer_reviews = IntegerField('Минимальное',
                                    [NumberRange(0, 10, message='количество решений должно быть от 0 до 10')],
                                    default=3, render_kw={'min': '0', 'max': '10', 'step': '1'})
    max_peer_reviews = IntegerField('Максимальное',
                                    [NumberRange(0, 10, message='количество решений должно быть от 0 до 10')],
                                    default=7, render_kw={'min': '0', 'max': '10', 'step': '1'})
    peer_review_criteria = TextAreaField('Описание критериев оценки при кросс-проверке',
                                         description='произвольный текст, поддерживается markdown, html и tex')
    peer_review_items = FieldList(FormField(ReviewItem, 'Критерии оценки'), min_entries=1)

    def validate(self):
        if not super().validate():
            return False

        result = True
        if self.middle_date.data:
            if self.middle_date.data <= self.start_date.data:
                self.middle_date.errors.append('промежуточный финиш должен быть после старта')
                result = False
            if self.finish_date.data <= self.middle_date.data:
                self.finish_date.errors.append('финиш должен быть после промежуточного финиша')
                result = False

        if self.finish_date.data <= self.start_date.data:
            self.finish_date.errors.append('финиш должен быть после старта')
            result = False

        if self.use_peer_review.data and self.peer_review_date.data <= self.finish_date.data:
            self.peer_review_date.errors.append('кросс-проверка должна быть после финиша')
            result = False

        if self.use_peer_review.data and self.min_peer_reviews.data > self.max_peer_reviews.data:
            self.max_peer_reviews.errors.append(
                'максимальное количество решений на кросс-проверку не может быть меньше минимального')
            result = False

        return result


class ImageForm(FlaskForm):
    name = StringField('Название образа',
                       [InputRequired('название изображения не может быть пустым')])
    build_script = TextAreaField('Скрипт для сборки образа',
                                 [InputRequired('скрипт для сборки не должен быть пустым')],
                                 render_kw={'style': 'font-family: monospace'})


class DeleteForm(FlaskForm):
    name = StringField('Подтвердите свое решение (можно скопировать)',
                       [InputRequired('решение не может быть пустым')])
