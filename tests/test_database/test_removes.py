from .database_setup_helper import DatabaseSetup


class TestRemovesAttachedTasks(DatabaseSetup):
    def test_delete_attached_task_1(self, postgres):
        """
        Создается 3 решения, 2 из них привязаны к одному файлу, 1 к другому.
        Удаляется прикрепление, к которому привязано 3 решения.
        Файл 1 присутствует как в прикреплении 1, так и в решении другого прикрепления 2.
        Файл 2 присутствует только в прикреплении 1.
        После удаления 1 прикрепления, остается 1 решение 2 прикрепления и 1 файл, привязанный к нему.
        Второе прикрепление удаляется успешно, остается файл 1, т.к. он есть в описании заданий.
        """
        from database import delete_attached_task

        user = self.add_user(postgres)
        rater = self.add_user(postgres)
        task = self.add_task(user, postgres)
        task2 = self.add_task(user, postgres)
        attached_task = self.add_attached_task(task, postgres)
        attached_task2 = self.add_attached_task(task2, postgres)
        course = self.add_course(postgres)
        group = self.add_group(course, postgres)
        self.add_group_tasks(group, attached_task, postgres)
        self.add_group_tasks(group, attached_task2, postgres)
        solution = self.add_solution(
            user, attached_task, course, rater, postgres
        )
        solution3 = self.add_solution(
            user, attached_task, course, rater, postgres
        )
        solution4 = self.add_solution(
            user, attached_task, course, rater, postgres
        )
        solution2 = self.add_solution(
            user, attached_task2, course, rater, postgres
        )
        file = self.add_file_metadata(postgres)
        file2 = self.add_file_metadata(postgres)
        solution_file11 = self.add_solution_file(solution, file, postgres)
        solution_file12 = self.add_solution_file(solution3, file, postgres)
        solution_file13 = self.add_solution_file(solution4, file2, postgres)
        solution_file21 = self.add_solution_file(solution2, file, postgres)
        task_file = self.add_task_file(task, file, postgres)
        task_file2 = self.add_task_file(task2, file, postgres)
        solution_review = self.add_solution_review(solution, rater, postgres)
        solution_review2 = self.add_solution_review(solution2, rater, postgres)

        delete_attached_task(attached_task.id)

        attached_tasks = postgres.session.execute(
            'SELECT * FROM attached_task',
        ).fetchall()
        assert attached_tasks[0].id == attached_task2.id
        assert len(attached_tasks) == 1

        group_tasks = postgres.session.execute(
            'SELECT * FROM group_tasks',
        ).fetchall()
        assert group_tasks[0].attached_task_id == attached_task2.id

        solutions = postgres.session.execute(
            'SELECT * FROM solution'
        ).fetchall()
        assert solutions[0].id == solution2.id

        solution_files = postgres.session.execute(
            'SELECT * FROM solution_file',
        ).fetchall()
        assert solution_files[0].id == solution_file21.id
        assert len(solution_files) == 1

        file_metadatas = postgres.session.execute(
            'SELECT * FROM file_metadata',
        ).fetchall()
        assert file_metadatas[0].id == file.id
        assert len(file_metadatas) == 1

        task_files = postgres.session.execute(
            'SELECT * FROM task_file',
        ).fetchall()
        assert len(task_files) == 2

        solution_reviews = postgres.session.execute(
            'SELECT * FROM solution_review',
        ).fetchall()
        assert solution_reviews[0].id == solution_review2.id
        assert len(solution_reviews) == 1

        delete_attached_task(attached_task2.id)

        file_metadatas = postgres.session.execute(
            'SELECT * FROM file_metadata',
        ).fetchall()
        assert file_metadatas[0].id == file.id
        assert len(file_metadatas) == 1

        task_files = postgres.session.execute(
            'SELECT * FROM task_file',
        ).fetchall()
        assert len(task_files) == 2

        solution_files = postgres.session.execute(
            'SELECT * FROM solution_file',
        ).fetchall()
        assert len(solution_files) == 0
        postgres.session.close()

    def test_delete_attached_task_2(self, postgres):
        """
        Создается 3 решения, 2 из них привязаны к одному файлу, 1 к другому.
        Файл 1 присутствует как в прикреплении 1, так и в решении другого прикрепления 2.
        Файл 2 присутствует в прикреплении 1, а также в описании задания 3 (task_file3).
        После удаления 1 прикрепления, остается 1 решение 2 прикрепления и 2 файла,
        один из которых привязан к нему, а другой - к описанию задания.
        """
        from database import delete_attached_task

        user = self.add_user(postgres)
        rater = self.add_user(postgres)
        task = self.add_task(user, postgres)
        task2 = self.add_task(user, postgres)
        attached_task = self.add_attached_task(task, postgres)
        attached_task2 = self.add_attached_task(task2, postgres)
        course = self.add_course(postgres)
        group = self.add_group(course, postgres)
        self.add_group_tasks(group, attached_task, postgres)
        self.add_group_tasks(group, attached_task2, postgres)
        solution11 = self.add_solution(
            user, attached_task, course, rater, postgres
        )
        solution12 = self.add_solution(
            user, attached_task, course, rater, postgres
        )
        solution13 = self.add_solution(
            user, attached_task, course, rater, postgres
        )
        solution21 = self.add_solution(
            user, attached_task2, course, rater, postgres
        )
        file = self.add_file_metadata(postgres)
        file2 = self.add_file_metadata(postgres)
        solution_file11 = self.add_solution_file(solution11, file, postgres)
        solution_file12 = self.add_solution_file(solution12, file, postgres)
        solution_file13 = self.add_solution_file(solution13, file2, postgres)
        solution_file21 = self.add_solution_file(solution21, file, postgres)
        task_file = self.add_task_file(task, file, postgres)
        task_file2 = self.add_task_file(task2, file, postgres)
        task_file3 = self.add_task_file(task2, file2, postgres)
        solution_review = self.add_solution_review(solution11, rater, postgres)
        solution_review2 = self.add_solution_review(
            solution21, rater, postgres
        )

        delete_attached_task(attached_task.id)

        attached_tasks = postgres.session.execute(
            'SELECT * FROM attached_task',
        ).fetchall()
        assert attached_tasks[0].id == attached_task2.id
        assert len(attached_tasks) == 1

        group_tasks = postgres.session.execute(
            'SELECT * FROM group_tasks',
        ).fetchall()
        assert group_tasks[0].attached_task_id == attached_task2.id

        solutions = postgres.session.execute(
            'SELECT * FROM solution'
        ).fetchall()
        assert solutions[0].id == solution21.id

        solution_files = postgres.session.execute(
            'SELECT * FROM solution_file',
        ).fetchall()
        assert solution_files[0].id == solution_file21.id
        assert len(solution_files) == 1

        file_metadatas = postgres.session.execute(
            'SELECT * FROM file_metadata',
        ).fetchall()
        assert file_metadatas[0].id == file.id
        assert file_metadatas[1].id == file2.id
        assert len(file_metadatas) == 2

        task_files = postgres.session.execute(
            'SELECT * FROM task_file',
        ).fetchall()
        assert len(task_files) == 3

        solution_reviews = postgres.session.execute(
            'SELECT * FROM solution_review',
        ).fetchall()
        assert solution_reviews[0].id == solution_review2.id
        assert len(solution_reviews) == 1
        postgres.session.close()

    def test_delete_attached_task_3(self, postgres):
        """
        Создается 3 решения, 2 из них привязаны к одному файлу, 1 к другому.
        Файл 1 присутствует только в прикреплении 1.
        Файл 2 прикреплен к решению в прикреплениях 1 и 2.
        Файл 3 присутствует только в описании трех заданий.
        После удаления 1 прикрепления, остается 1 решение, 1 прикрепление,
        файлы 2 и 3, а файл 1 удаляется.
        Всего 3 task_files.
        """
        from database import delete_attached_task

        user = self.add_user(postgres)
        rater = self.add_user(postgres)
        task = self.add_task(user, postgres)
        task2 = self.add_task(user, postgres)
        attached_task = self.add_attached_task(task, postgres)
        attached_task2 = self.add_attached_task(task2, postgres)
        course = self.add_course(postgres)
        group = self.add_group(course, postgres)
        self.add_group_tasks(group, attached_task, postgres)
        self.add_group_tasks(group, attached_task2, postgres)
        solution11 = self.add_solution(
            user, attached_task, course, rater, postgres
        )
        solution12 = self.add_solution(
            user, attached_task, course, rater, postgres
        )
        solution13 = self.add_solution(
            user, attached_task, course, rater, postgres
        )
        solution21 = self.add_solution(
            user, attached_task2, course, rater, postgres
        )
        file = self.add_file_metadata(postgres)
        file2 = self.add_file_metadata(postgres)
        file3 = self.add_file_metadata(postgres)
        solution_file11 = self.add_solution_file(solution11, file, postgres)
        solution_file12 = self.add_solution_file(solution12, file, postgres)
        solution_file13 = self.add_solution_file(solution13, file2, postgres)
        solution_file21 = self.add_solution_file(solution21, file2, postgres)
        task_file = self.add_task_file(task, file3, postgres)
        task_file2 = self.add_task_file(task2, file3, postgres)
        task_file3 = self.add_task_file(task2, file3, postgres)
        solution_review = self.add_solution_review(solution11, rater, postgres)
        solution_review2 = self.add_solution_review(
            solution21, rater, postgres
        )

        delete_attached_task(attached_task.id)

        attached_tasks = postgres.session.execute(
            'SELECT * FROM attached_task',
        ).fetchall()
        assert attached_tasks[0].id == attached_task2.id
        assert len(attached_tasks) == 1

        group_tasks = postgres.session.execute(
            'SELECT * FROM group_tasks',
        ).fetchall()
        assert group_tasks[0].attached_task_id == attached_task2.id

        solutions = postgres.session.execute(
            'SELECT * FROM solution'
        ).fetchall()
        assert solutions[0].id == solution21.id

        solution_files = postgres.session.execute(
            'SELECT * FROM solution_file',
        ).fetchall()
        assert solution_file21.id == solution_files[0].id
        assert len(solution_files) == 1

        file_metadatas = postgres.session.execute(
            'SELECT * FROM file_metadata',
        ).fetchall()
        assert file_metadatas[0].id == file2.id
        assert file_metadatas[1].id == file3.id
        assert len(file_metadatas) == 2

        task_files = postgres.session.execute(
            'SELECT * FROM task_file',
        ).fetchall()
        assert len(task_files) == 3
        assert task_files[0].file_id == file3.id
        assert task_files[1].file_id == file3.id
        assert task_files[2].file_id == file3.id

        solution_reviews = postgres.session.execute(
            'SELECT * FROM solution_review',
        ).fetchall()
        assert solution_reviews[0].id == solution_review2.id
        assert len(solution_reviews) == 1
        postgres.session.close()

    def test_delete_attached_task_4(self, postgres):
        from database import delete_attached_task

        user = self.add_user(postgres)
        rater = self.add_user(postgres)
        task = self.add_task(user, postgres)
        task2 = self.add_task(user, postgres)
        attached_task = self.add_attached_task(task, postgres)
        attached_task2 = self.add_attached_task(task2, postgres)
        course = self.add_course(postgres)
        group = self.add_group(course, postgres)
        self.add_group_tasks(group, attached_task, postgres)
        self.add_group_tasks(group, attached_task2, postgres)
        solution11 = self.add_solution(
            user, attached_task, course, rater, postgres
        )
        solution12 = self.add_solution(
            user, attached_task, course, rater, postgres
        )
        solution13 = self.add_solution(
            user, attached_task, course, rater, postgres
        )
        solution21 = self.add_solution(
            user, attached_task2, course, rater, postgres
        )
        file = self.add_file_metadata(postgres)
        file2 = self.add_file_metadata(postgres)
        file3 = self.add_file_metadata(postgres)
        solution_file11 = self.add_solution_file(solution11, file, postgres)
        solution_file12 = self.add_solution_file(solution12, file, postgres)
        solution_file13 = self.add_solution_file(solution13, file2, postgres)
        solution_file21 = self.add_solution_file(solution21, file2, postgres)
        task_file = self.add_task_file(task, file3, postgres)
        task_file2 = self.add_task_file(task2, file3, postgres)
        task_file3 = self.add_task_file(task2, file3, postgres)
        solution_review = self.add_solution_review(solution11, rater, postgres)
        solution_review2 = self.add_solution_review(
            solution21, rater, postgres
        )

        delete_attached_task(attached_task2.id)

        attached_tasks = postgres.session.execute(
            'SELECT * FROM attached_task',
        ).fetchall()
        assert attached_tasks[0].id == attached_task.id
        assert len(attached_tasks) == 1

        group_tasks = postgres.session.execute(
            'SELECT * FROM group_tasks',
        ).fetchall()
        assert group_tasks[0].attached_task_id == attached_task.id

        solutions = postgres.session.execute(
            'SELECT * FROM solution'
        ).fetchall()
        assert solutions[0].id == solution11.id
        assert solutions[1].id == solution12.id
        assert solutions[2].id == solution13.id
        assert len(solutions) == 3

        solution_files = postgres.session.execute(
            'SELECT * FROM solution_file',
        ).fetchall()
        assert solution_files[0].file_id == file.id
        assert solution_files[1].file_id == file.id
        assert solution_files[2].file_id == file2.id
        assert len(solution_files) == 3

        file_metadatas = postgres.session.execute(
            'SELECT * FROM file_metadata',
        ).fetchall()
        assert file_metadatas[0].id == file.id
        assert file_metadatas[1].id == file2.id
        assert file_metadatas[2].id == file3.id
        assert len(file_metadatas) == 3

        task_files = postgres.session.execute(
            'SELECT * FROM task_file',
        ).fetchall()
        assert len(task_files) == 3
        assert task_files[0].file_id == file3.id
        assert task_files[1].file_id == file3.id
        assert task_files[2].file_id == file3.id

        solution_reviews = postgres.session.execute(
            'SELECT * FROM solution_review',
        ).fetchall()
        assert solution_reviews[0].id == solution_review.id
        assert len(solution_reviews) == 1
        postgres.session.close()


class TestRemovesTasks(DatabaseSetup):
    def test_delete_task_1(self, postgres):
        """
        Создается 1 задание, 1 прикрепление, 1 решение, 1 прикрепление к решению,
        1 файл, 1 прикрепление файла к заданию, 1 прикрепление файла к решению,
        1 оценка решения, 1 ревью решения.
        После удаления задания, удаляются все связанные с ним объекты.
        """
        from database import delete_task, Task
        user = self.add_user(postgres)
        rater = self.add_user(postgres)
        task = self.add_task(user, postgres)
        attached_task = self.add_attached_task(task, postgres)
        course = self.add_course(postgres)
        group = self.add_group(course, postgres)
        self.add_group_tasks(group, attached_task, postgres)
        solution = self.add_solution(
            user, attached_task, course, rater, postgres
        )
        file = self.add_file_metadata(postgres)
        solution_file = self.add_solution_file(solution, file, postgres)
        task_file = self.add_task_file(task, file, postgres)
        solution_review = self.add_solution_review(solution, rater, postgres)

        task = Task.query.get_or_404(task.id)
        delete_task(task)

        tasks = postgres.session.execute(
            'SELECT * FROM task',
        ).fetchall()
        assert len(tasks) == 0

        attached_tasks = postgres.session.execute(
            'SELECT * FROM attached_task',
        ).fetchall()
        assert len(attached_tasks) == 0

        group_tasks = postgres.session.execute(
            'SELECT * FROM group_tasks',
        ).fetchall()
        assert len(group_tasks) == 0

        solutions = postgres.session.execute(
            'SELECT * FROM solution'
        ).fetchall()
        assert len(solutions) == 0

        solution_files = postgres.session.execute(
            'SELECT * FROM solution_file',
        ).fetchall()
        assert len(solution_files) == 0

        file_metadatas = postgres.session.execute(
            'SELECT * FROM file_metadata',
        ).fetchall()
        assert len(file_metadatas) == 0

        task_files = postgres.session.execute(
            'SELECT * FROM task_file',
        ).fetchall()
        assert len(task_files) == 0

        solution_reviews = postgres.session.execute(
            'SELECT * FROM solution_review',
        ).fetchall()
        assert len(solution_reviews) == 0
        postgres.session.close()

    def test_delete_task_2(self, postgres):
        from database import delete_task, Task
        user = self.add_user(postgres)
        rater = self.add_user(postgres)
        task = self.add_task(user, postgres)
        attached_task = self.add_attached_task(task, postgres)
        attached_task2 = self.add_attached_task(task, postgres)
        course = self.add_course(postgres)
        group = self.add_group(course, postgres)
        self.add_group_tasks(group, attached_task, postgres)
        self.add_group_tasks(group, attached_task2, postgres)
        solution = self.add_solution(
            user, attached_task, course, rater, postgres
        )
        solution2 = self.add_solution(
            user, attached_task2, course, rater, postgres
        )
        file1 = self.add_file_metadata(postgres)
        file2 = self.add_file_metadata(postgres)
        file3 = self.add_file_metadata(postgres)
        solution_file = self.add_solution_file(solution, file1, postgres)
        solution_file2 = self.add_solution_file(solution2, file3, postgres)
        task_file = self.add_task_file(task, file2, postgres)
        task_file2 = self.add_task_file(task, file3, postgres)
        solution_review = self.add_solution_review(solution, rater, postgres)

        task = Task.query.get_or_404(task.id)
        delete_task(task)

        tasks = postgres.session.execute(
            'SELECT * FROM task',
        ).fetchall()
        assert len(tasks) == 0

        attached_tasks = postgres.session.execute(
            'SELECT * FROM attached_task',
        ).fetchall()
        assert len(attached_tasks) == 0

        group_tasks = postgres.session.execute(
            'SELECT * FROM group_tasks',
        ).fetchall()
        assert len(group_tasks) == 0

        solutions = postgres.session.execute(
            'SELECT * FROM solution'
        ).fetchall()
        assert len(solutions) == 0

        solution_files = postgres.session.execute(
            'SELECT * FROM solution_file',
        ).fetchall()
        assert len(solution_files) == 0

        file_metadatas = postgres.session.execute(
            'SELECT * FROM file_metadata',
        ).fetchall()
        assert len(file_metadatas) == 0

        task_files = postgres.session.execute(
            'SELECT * FROM task_file',
        ).fetchall()
        assert len(task_files) == 0

        solution_reviews = postgres.session.execute(
            'SELECT * FROM solution_review',
        ).fetchall()
        assert len(solution_reviews) == 0
        postgres.session.close()

    def test_delete_task_3(self, postgres):
        from database import delete_task, Task
        user = self.add_user(postgres)
        rater = self.add_user(postgres)
        task = self.add_task(user, postgres)
        task2 = self.add_task(user, postgres)
        attached_task = self.add_attached_task(task, postgres)
        attached_task2 = self.add_attached_task(task, postgres)
        attached_task3 = self.add_attached_task(task2, postgres)
        course = self.add_course(postgres)
        group = self.add_group(course, postgres)
        self.add_group_tasks(group, attached_task, postgres)
        self.add_group_tasks(group, attached_task2, postgres)
        self.add_group_tasks(group, attached_task3, postgres)
        solution = self.add_solution(
            user, attached_task, course, rater, postgres
        )
        solution2 = self.add_solution(
            user, attached_task2, course, rater, postgres
        )
        solution3 = self.add_solution(
            user, attached_task3, course, rater, postgres
        )
        file1 = self.add_file_metadata(postgres)
        file2 = self.add_file_metadata(postgres)
        file3 = self.add_file_metadata(postgres)
        file4 = self.add_file_metadata(postgres)
        file5 = self.add_file_metadata(postgres)
        file6 = self.add_file_metadata(postgres)
        solution_file = self.add_solution_file(solution, file1, postgres)
        solution_file2 = self.add_solution_file(solution2, file3, postgres)
        solution_file3 = self.add_solution_file(solution2, file4, postgres)
        solution_file5 = self.add_solution_file(solution, file5, postgres)
        solution_file6 = self.add_solution_file(solution3, file5, postgres)
        task_file = self.add_task_file(task, file2, postgres)
        task_file2 = self.add_task_file(task, file3, postgres)
        task_file3 = self.add_task_file(task2, file4, postgres)
        task_file4 = self.add_task_file(task, file6, postgres)
        task_file5 = self.add_task_file(task2, file6, postgres)
        solution_review = self.add_solution_review(solution, rater, postgres)

        task_to_delete = Task.query.get_or_404(task.id)
        delete_task(task_to_delete)

        tasks = postgres.session.execute(
            'SELECT * FROM task',
        ).fetchall()
        assert len(tasks) == 1
        assert tasks[0].id == task2.id

        attached_tasks = postgres.session.execute(
            'SELECT * FROM attached_task',
        ).fetchall()
        assert len(attached_tasks) == 1
        assert attached_tasks[0].id == attached_task3.id

        group_tasks = postgres.session.execute(
            'SELECT * FROM group_tasks',
        ).fetchall()
        assert len(group_tasks) == 1
        assert group_tasks[0].attached_task_id == attached_task3.id

        solutions = postgres.session.execute(
            'SELECT * FROM solution'
        ).fetchall()
        assert len(solutions) == 1
        assert solutions[0].id == solution3.id

        solution_files = postgres.session.execute(
            'SELECT * FROM solution_file',
        ).fetchall()
        assert len(solution_files) == 1
        assert solution_files[0].solution_id == solution3.id

        file_metadatas = postgres.session.execute(
            'SELECT * FROM file_metadata',
        ).fetchall()
        assert len(file_metadatas) == 3
        assert file_metadatas[0].id == file4.id
        assert file_metadatas[1].id == file5.id
        assert file_metadatas[2].id == file6.id

        task_files = postgres.session.execute(
            'SELECT * FROM task_file',
        ).fetchall()
        assert len(task_files) == 2
        assert task_files[0].task_id == task2.id
        assert task_files[1].task_id == task2.id
        assert task_files[0].file_id == file4.id
        assert task_files[1].file_id == file6.id

        solution_reviews = postgres.session.execute(
            'SELECT * FROM solution_review',
        ).fetchall()
        assert len(solution_reviews) == 0
        postgres.session.close()

    def test_delete_task_4(self, postgres):
        from database import delete_task, Task
        user = self.add_user(postgres)
        rater = self.add_user(postgres)
        task = self.add_task(user, postgres)
        task2 = self.add_task(user, postgres)
        attached_task = self.add_attached_task(task, postgres)
        attached_task2 = self.add_attached_task(task, postgres)
        attached_task3 = self.add_attached_task(task2, postgres)
        course = self.add_course(postgres)
        group = self.add_group(course, postgres)
        self.add_group_tasks(group, attached_task, postgres)
        self.add_group_tasks(group, attached_task2, postgres)
        self.add_group_tasks(group, attached_task3, postgres)
        solution = self.add_solution(
            user, attached_task, course, rater, postgres
        )
        solution2 = self.add_solution(
            user, attached_task2, course, rater, postgres
        )
        solution3 = self.add_solution(
            user, attached_task3, course, rater, postgres
        )
        file1 = self.add_file_metadata(postgres)
        file2 = self.add_file_metadata(postgres)
        file3 = self.add_file_metadata(postgres)
        file4 = self.add_file_metadata(postgres)
        file5 = self.add_file_metadata(postgres)
        file6 = self.add_file_metadata(postgres)
        solution_file = self.add_solution_file(solution, file1, postgres)
        solution_file2 = self.add_solution_file(solution2, file3, postgres)
        solution_file3 = self.add_solution_file(solution2, file4, postgres)
        solution_file5 = self.add_solution_file(solution, file5, postgres)
        solution_file6 = self.add_solution_file(solution3, file5, postgres)
        task_file = self.add_task_file(task, file2, postgres)
        task_file2 = self.add_task_file(task, file3, postgres)
        task_file3 = self.add_task_file(task2, file4, postgres)
        task_file4 = self.add_task_file(task, file6, postgres)
        task_file5 = self.add_task_file(task2, file6, postgres)
        solution_review = self.add_solution_review(solution, rater, postgres)
        print(f"task.id: {task.id}, task2.id: {task2.id}")
        print(f"file1: {file1.id}, "
              f"file2: {file2.id}, "
              f"file3: {file3.id}, "
              f"file4: {file4.id}, "
              f"file5: {file5.id}, "
              f"file6: {file6.id}")

        task_to_delete = Task.query.get_or_404(task2.id)
        delete_task(task_to_delete)

        tasks = postgres.session.execute(
            'SELECT * FROM task',
        ).fetchall()
        assert len(tasks) == 1
        assert tasks[0].id == task.id

        attached_tasks = postgres.session.execute(
            'SELECT * FROM attached_task',
        ).fetchall()
        assert len(attached_tasks) == 2
        assert attached_tasks[0].id == attached_task.id
        assert attached_tasks[1].id == attached_task2.id

        group_tasks = postgres.session.execute(
            'SELECT * FROM group_tasks',
        ).fetchall()
        assert len(group_tasks) == 2
        assert group_tasks[0].attached_task_id == attached_task.id
        assert group_tasks[1].attached_task_id == attached_task2.id

        solutions = postgres.session.execute(
            'SELECT * FROM solution'
        ).fetchall()
        assert len(solutions) == 2
        assert solutions[0].id == solution.id
        assert solutions[1].id == solution2.id

        solution_files = postgres.session.execute(
            'SELECT * FROM solution_file',
        ).fetchall()
        assert len(solution_files) == 4
        assert solution_files[0].file_id == file1.id
        assert solution_files[1].file_id == file3.id
        assert solution_files[2].file_id == file4.id
        assert solution_files[3].file_id == file5.id

        file_metadatas = postgres.session.execute(
            'SELECT * FROM file_metadata',
        ).fetchall()
        assert len(file_metadatas) == 6
        assert file_metadatas[0].id == file1.id
        assert file_metadatas[1].id == file2.id
        assert file_metadatas[2].id == file3.id
        assert file_metadatas[3].id == file4.id
        assert file_metadatas[4].id == file5.id
        assert file_metadatas[5].id == file6.id

        task_files = postgres.session.execute(
            'SELECT * FROM task_file',
        ).fetchall()
        assert len(task_files) == 3
        assert task_files[0].file_id == file2.id
        assert task_files[1].file_id == file3.id
        assert task_files[2].file_id == file6.id

        solution_reviews = postgres.session.execute(
            'SELECT * FROM solution_review',
        ).fetchall()
        assert len(solution_reviews) == 1
        assert solution_reviews[0].id == solution_review.id
        postgres.session.close()

    def test_delete_task_5(self, postgres):
        from database import delete_task, Task

        user = self.add_user(postgres)
        rater = self.add_user(postgres)
        task = self.add_task(user, postgres)
        task2 = self.add_task(user, postgres)
        attached_task = self.add_attached_task(task, postgres)
        attached_task2 = self.add_attached_task(task2, postgres)
        course = self.add_course(postgres)
        group = self.add_group(course, postgres)
        self.add_group_tasks(group, attached_task, postgres)
        self.add_group_tasks(group, attached_task2, postgres)
        solution11 = self.add_solution(
            user, attached_task, course, rater, postgres
        )
        solution12 = self.add_solution(
            user, attached_task, course, rater, postgres
        )
        solution13 = self.add_solution(
            user, attached_task, course, rater, postgres
        )
        solution21 = self.add_solution(
            user, attached_task2, course, rater, postgres
        )
        file = self.add_file_metadata(postgres)
        file2 = self.add_file_metadata(postgres)
        file3 = self.add_file_metadata(postgres)
        solution_file11 = self.add_solution_file(solution11, file, postgres)
        solution_file12 = self.add_solution_file(solution12, file, postgres)
        solution_file13 = self.add_solution_file(solution13, file2, postgres)
        solution_file21 = self.add_solution_file(solution21, file2, postgres)
        task_file = self.add_task_file(task, file3, postgres)
        task_file2 = self.add_task_file(task2, file3, postgres)
        task_file3 = self.add_task_file(task2, file3, postgres)
        solution_review = self.add_solution_review(solution11, rater, postgres)
        solution_review2 = self.add_solution_review(
            solution21, rater, postgres
        )

        task_to_delete = Task.query.get_or_404(task2.id)
        delete_task(task_to_delete)

        tasks = postgres.session.execute(
            'SELECT * FROM task',
        ).fetchall()
        assert len(tasks) == 1
        assert tasks[0].id == task.id

        attached_tasks = postgres.session.execute(
            'SELECT * FROM attached_task',
        ).fetchall()
        assert len(attached_tasks) == 1
        assert attached_tasks[0].id == attached_task.id

        group_tasks = postgres.session.execute(
            'SELECT * FROM group_tasks',
        ).fetchall()
        assert len(group_tasks) == 1
        assert group_tasks[0].attached_task_id == attached_task.id

        solutions = postgres.session.execute(
            'SELECT * FROM solution'
        ).fetchall()
        assert len(solutions) == 3
        assert solutions[0].id == solution11.id
        assert solutions[1].id == solution12.id
        assert solutions[2].id == solution13.id

        solution_files = postgres.session.execute(
            'SELECT * FROM solution_file',
        ).fetchall()
        assert len(solution_files) == 3
        assert solution_files[0].file_id == file.id
        assert solution_files[1].file_id == file.id
        assert solution_files[2].file_id == file2.id

        file_metadatas = postgres.session.execute(
            'SELECT * FROM file_metadata',
        ).fetchall()
        assert len(file_metadatas) == 3
        assert file_metadatas[0].id == file.id
        assert file_metadatas[1].id == file2.id
        assert file_metadatas[2].id == file3.id

        task_files = postgres.session.execute(
            'SELECT * FROM task_file',
        ).fetchall()
        assert len(task_files) == 1
        assert task_files[0].file_id == file3.id

        solution_reviews = postgres.session.execute(
            'SELECT * FROM solution_review',
        ).fetchall()
        assert len(solution_reviews) == 1
        assert solution_reviews[0].id == solution_review.id
        postgres.session.close()

    def test_delete_task_6(self, postgres):
        from database import delete_task, Task

        user = self.add_user(postgres)
        rater = self.add_user(postgres)
        task = self.add_task(user, postgres)
        task2 = self.add_task(user, postgres)
        attached_task = self.add_attached_task(task, postgres)
        attached_task2 = self.add_attached_task(task2, postgres)
        course = self.add_course(postgres)
        group = self.add_group(course, postgres)
        self.add_group_tasks(group, attached_task, postgres)
        self.add_group_tasks(group, attached_task2, postgres)
        solution11 = self.add_solution(
            user, attached_task, course, rater, postgres
        )
        solution12 = self.add_solution(
            user, attached_task, course, rater, postgres
        )
        solution13 = self.add_solution(
            user, attached_task, course, rater, postgres
        )
        solution21 = self.add_solution(
            user, attached_task2, course, rater, postgres
        )
        file = self.add_file_metadata(postgres)
        file2 = self.add_file_metadata(postgres)
        file3 = self.add_file_metadata(postgres)
        solution_file11 = self.add_solution_file(solution11, file, postgres)
        solution_file12 = self.add_solution_file(solution12, file, postgres)
        solution_file13 = self.add_solution_file(solution13, file2, postgres)
        solution_file21 = self.add_solution_file(solution21, file2, postgres)
        task_file = self.add_task_file(task, file3, postgres)
        task_file2 = self.add_task_file(task2, file3, postgres)
        task_file3 = self.add_task_file(task2, file3, postgres)
        solution_review = self.add_solution_review(solution11, rater, postgres)
        solution_review2 = self.add_solution_review(
            solution21, rater, postgres
        )

        task_to_delete = Task.query.get_or_404(task.id)
        delete_task(task_to_delete)

        tasks = postgres.session.execute(
            'SELECT * FROM task',
        ).fetchall()
        assert len(tasks) == 1
        assert tasks[0].id == task2.id

        attached_tasks = postgres.session.execute(
            'SELECT * FROM attached_task',
        ).fetchall()
        assert len(attached_tasks) == 1
        assert attached_tasks[0].id == attached_task2.id

        group_tasks = postgres.session.execute(
            'SELECT * FROM group_tasks',
        ).fetchall()
        assert len(group_tasks) == 1
        assert group_tasks[0].attached_task_id == attached_task2.id

        solutions = postgres.session.execute(
            'SELECT * FROM solution'
        ).fetchall()
        assert len(solutions) == 1
        assert solutions[0].id == solution21.id

        solution_files = postgres.session.execute(
            'SELECT * FROM solution_file',
        ).fetchall()
        assert len(solution_files) == 1
        assert solution_files[0].file_id == file2.id

        file_metadatas = postgres.session.execute(
            'SELECT * FROM file_metadata',
        ).fetchall()
        assert len(file_metadatas) == 2
        assert file_metadatas[0].id == file2.id
        assert file_metadatas[1].id == file3.id

        task_files = postgres.session.execute(
            'SELECT * FROM task_file',
        ).fetchall()
        assert len(task_files) == 2
        assert task_files[0].file_id == file3.id
        assert task_files[1].file_id == file3.id

        solution_reviews = postgres.session.execute(
            'SELECT * FROM solution_review',
        ).fetchall()
        assert len(solution_reviews) == 1
        assert solution_reviews[0].id == solution_review2.id
        postgres.session.close()


class TestRemovesCourses(DatabaseSetup):
    def test_delete_course_1(self, postgres):
        from database import delete_course, Course

        course = self.add_course(postgres)
        course2 = self.add_course(postgres)
        course_to_delete = Course.query.get_or_404(course.id)
        delete_course(course_to_delete)

        courses = postgres.session.execute(
            'SELECT * FROM course',
        ).fetchall()
        assert len(courses) == 1
        assert courses[0].id == course2.id
        postgres.session.close()

    def test_delete_full_course(self, postgres):
        from database import delete_course, Course

        user = self.add_user(postgres)
        course = self.add_course(postgres)
        course2 = self.add_course(postgres)
        group = self.add_group(course, postgres)
        group2 = self.add_group(course2, postgres)
        task = self.add_task(user, postgres)
        task2 = self.add_task(user, postgres)
        attached_task = self.add_attached_task(task, postgres)
        attached_task2 = self.add_attached_task(task2, postgres)
        attached_task22 = self.add_attached_task(task2, postgres)
        self.add_group_tasks(group, attached_task, postgres)
        self.add_group_tasks(group, attached_task2, postgres)
        self.add_group_tasks(group2, attached_task22, postgres)
        solution = self.add_solution(user, attached_task, course, user, postgres)
        solution2 = self.add_solution(user, attached_task2, course, user, postgres)
        solution22 = self.add_solution(user, attached_task22, course2, user, postgres)
        file = self.add_file_metadata(postgres)
        file2 = self.add_file_metadata(postgres)
        file3 = self.add_file_metadata(postgres)
        solution_file = self.add_solution_file(solution, file, postgres)
        solution_file2 = self.add_solution_file(solution2, file, postgres)
        solution_file3 = self.add_solution_file(solution22, file2, postgres)
        task_file = self.add_task_file(task, file3, postgres)
        task_file2 = self.add_task_file(task2, file3, postgres)
        task_file3 = self.add_task_file(task2, file3, postgres)
        solution_review = self.add_solution_review(solution, user, postgres)
        solution_review2 = self.add_solution_review(solution2, user, postgres)
        solution_review3 = self.add_solution_review(solution22, user, postgres)
        user_info = self.add_user_info(user, course, postgres)
        user_info2 = self.add_user_info(user, course2, postgres)
        standings1 = self.add_standing(user, course, postgres)
        standings2 = self.add_standing(user, course2, postgres)
        user_role = self.add_user_role(user, course, postgres)
        user_role2 = self.add_user_role(user, course2, postgres)
        self.add_group_students(user, group, postgres)
        self.add_group_students(user, group2, postgres)

        course_to_delete = Course.query.get_or_404(course.id)
        delete_course(course_to_delete)

        courses = postgres.session.execute(
            'SELECT * FROM course',
        ).fetchall()
        assert len(courses) == 1
        assert courses[0].id == course2.id

        groups = postgres.session.execute(
            'SELECT * FROM "group"',
        ).fetchall()
        assert len(groups) == 1
        assert groups[0].id == group2.id

        tasks = postgres.session.execute(
            'SELECT * FROM task',
        ).fetchall()
        assert len(tasks) == 2
        assert tasks[0].id == task.id
        assert tasks[1].id == task2.id

        attached_tasks = postgres.session.execute(
            'SELECT * FROM attached_task',
        ).fetchall()
        assert len(attached_tasks) == 1
        assert attached_tasks[0].id == attached_task22.id

        group_tasks = postgres.session.execute(
            'SELECT * FROM group_tasks',
        ).fetchall()
        assert len(group_tasks) == 1
        assert group_tasks[0].group_id == group2.id

        solutions = postgres.session.execute(
            'SELECT * FROM solution',
        ).fetchall()
        assert len(solutions) == 1
        assert solutions[0].id == solution22.id

        solution_files = postgres.session.execute(
            'SELECT * FROM solution_file',
        ).fetchall()
        assert len(solution_files) == 1
        assert solution_files[0].file_id == file2.id

        file_metadatas = postgres.session.execute(
            'SELECT * FROM file_metadata',
        ).fetchall()
        assert len(file_metadatas) == 2
        assert file_metadatas[0].id == file2.id
        assert file_metadatas[1].id == file3.id

        task_files = postgres.session.execute(
            'SELECT * FROM task_file',
        ).fetchall()
        assert len(task_files) == 3
        assert task_files[0].file_id == file3.id
        assert task_files[1].file_id == file3.id
        assert task_files[2].file_id == file3.id

        solution_reviews = postgres.session.execute(
            'SELECT * FROM solution_review',
        ).fetchall()
        assert len(solution_reviews) == 1
        assert solution_reviews[0].id == solution_review3.id

        user_infos = postgres.session.execute(
            'SELECT * FROM user_info',
        ).fetchall()
        assert len(user_infos) == 1
        assert user_infos[0].id == user_info2.id

        standings = postgres.session.execute(
            'SELECT * FROM standings',
        ).fetchall()
        assert len(standings) == 1
        assert standings[0].id == standings2.id

        user_roles = postgres.session.execute(
            'SELECT * FROM user_role',
        ).fetchall()
        assert len(user_roles) == 1
        assert user_roles[0].id == user_role2.id

        group_students = postgres.session.execute(
            'SELECT * FROM group_students',
        ).fetchall()
        assert len(group_students) == 1
        assert group_students[0].group_id == group2.id
        postgres.session.close()
