from os import environ

import pytest
import simplejson
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy_utils import create_database, database_exists, drop_database

from app_.config import get_settings


@pytest.fixture()
def postgres() -> str:
    """
    Создает временную БД для запуска теста.
    """

    settings = get_settings()
    settings.POSTGRES_DB = 'db_for_test.pytest'
    environ['POSTGRES_DB'] = settings.POSTGRES_DB

    from app import app
    from database import init_db

    if not database_exists(settings.database_uri_sync):
        create_database(settings.database_uri_sync)
    db: SQLAlchemy = SQLAlchemy(
        app,
        session_options={'autoflush': False},
        engine_options={'json_serializer': simplejson.dumps},
    )
    try:
        init_db()
        yield db
    finally:
        db.reflect()
        db.drop_all()


@pytest.fixture()
def client(postgres):
    """
    SQLAlchemy engine, bound to temporary database.
    """
    from app import app

    WTF_CSRF_ENABLED = app.config.get('WTF_CSRF_ENABLED')
    TESTING = app.config.get('TESTING')

    app.config['WTF_CSRF_ENABLED'] = False
    app.config['TESTING'] = True

    with app.test_client() as test_client:
        yield test_client

    app.config['WTF_CSRF_ENABLED'] = WTF_CSRF_ENABLED
    app.config['TESTING'] = TESTING


@pytest.fixture()
def user_logged_in(client):
    r = client.post(
        '/login', data={'email': 'test@example.com', 'password': '12345'}
    )
    from flask import get_flashed_messages

    flashed_messages = get_flashed_messages()
    print(flashed_messages)
    print(r)


@pytest.fixture(autouse=True, scope='session')
def _drop_tmp_db():
    """
    Удаляет временную БД после завершения теста.
    """
    settings = get_settings()
    settings.POSTGRES_DB = 'db_for_test.pytest'
    yield
    if database_exists(settings.database_uri_sync):
        drop_database(settings.database_uri_sync)
