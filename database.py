from app import db
from asteval import Interpreter
from datetime import datetime
from flask_login import UserMixin
from math import ceil
from sqlalchemy.ext.associationproxy import association_proxy
from sqlalchemy.orm import joinedload
from sqlalchemy.orm.collections import attribute_mapped_collection
from werkzeug.utils import cached_property
import os
import typing as tp

group_students = db.Table('group_students',
                          db.Column('user_id', db.Integer, db.ForeignKey('user.id')),
                          db.Column('group_id', db.Integer, db.ForeignKey('group.id')),
                          )


class ARMixin(object):
    def save(self):
        db.session.add(self)
        self._flush()
        return self

    def update(self, **kwargs):
        for attr, value in kwargs.items():
            setattr(self, attr, value)
        return self.save()

    def delete(self):
        db.session.delete(self)
        self._flush()

    @staticmethod
    def _flush():
        try:
            db.session.commit()
        except Exception as error:
            db.session.rollback()
            raise


class User(db.Model, UserMixin, ARMixin):
    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String(128), unique=True, nullable=False)
    password = db.Column(db.String(128), nullable=False)
    firstname = db.Column(db.String(128), nullable=False)
    lastname = db.Column(db.String(128), nullable=False)
    patronym = db.Column(db.String(128))
    email_confirmed = db.Column(db.Boolean(), default=False)
    confirm_id = db.Column(db.String(128), default='')
    is_admin = db.Column(db.Boolean(), nullable=False, default=False)
    groups = db.relationship('Group', secondary=group_students,
                             backref=db.backref('students', lazy='dynamic'))
    roles = db.relationship('UserRole', backref='user')

    @cached_property
    def courses(self):
        return Course.query \
            .join(Group.students).filter(User.id == self.id) \
            .join(Course).order_by(Course.id).all()

    def get_course_or_404(self, course_id):
        course = Course.query \
            .join(Group.students).filter(User.id == self.id) \
            .join(Course).filter(Course.id == course_id).first_or_404()
        course.user_id = self.id
        course.hide_tasks_before_deadline = not self.is_admin
        return course

    def is_task_availiable(self, task_id):
        attached_task = AttachedTask.query \
            .join(Group.students).filter(User.id == self.id) \
            .join(Group.attached_tasks).filter(AttachedTask.task_id == task_id) \
            .first()
        return attached_task is not None

    def get_id(self):
        return str(self.id)


class UserInfo(db.Model, ARMixin):
    id = db.Column(db.Integer, primary_key=True)
    info = db.Column(db.String(128), nullable=True)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    user = db.relationship('User')
    course_id = db.Column(db.Integer, db.ForeignKey('course.id'))
    course = db.relationship('Course')


class UserRole(db.Model, ARMixin):
    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    course_id = db.Column(db.Integer, db.ForeignKey('course.id'))
    role = db.Column(db.String(128))


class Course(db.Model, ARMixin):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(128), nullable=False)
    short_name = db.Column(db.String(128), nullable=False)
    info = db.Column(db.String(65536))
    user_info_name = db.Column(db.String(128))
    user_info_help = db.Column(db.String(128))
    user_info_regex = db.Column(db.String(128))
    groups = db.relationship('Group', order_by='Group.id', backref='course',
                             lazy='joined')
    roles = db.relationship('UserRole', order_by='UserRole.id', backref='course')

    @cached_property
    def students(self):
        return User.query \
            .join(Group.students).join(Course) \
            .filter(Course.id == self.id).order_by(User.id).all()

    @cached_property
    def tasks(self):
        tasks = AttachedTask.query \
            .join(Group.students).filter(User.id == self.user_id) \
            .join(Group.attached_tasks).join(Course) \
            .filter(Course.id == self.id).order_by(AttachedTask.id)
        if self.hide_tasks_before_deadline:
            tasks = tasks.filter(AttachedTask.start_date <= datetime.now())
        return tasks.all()


group_tasks = db.Table('group_tasks',
                       db.Column('group_id', db.Integer, db.ForeignKey('group.id')),
                       db.Column('attached_task_id', db.Integer, db.ForeignKey('attached_task.id')),
                       )


class SolutionReview(db.Model, ARMixin):
    id = db.Column(db.Integer, primary_key=True)
    date = db.Column(db.DateTime, default=datetime.now)

    rater_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    rater = db.relationship('User')

    solution_id = db.Column(db.Integer, db.ForeignKey('solution.id'))
    solution = db.relationship('Solution')

    rater_comment = db.Column(db.String(1048576))
    review_rules = db.Column(db.JSON)
    is_review_finished = db.Column(db.Boolean(), default=False)
    mark = db.Column(db.Float)


class Group(db.Model, ARMixin):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(128), nullable=False)
    course_id = db.Column(db.Integer, db.ForeignKey('course.id'))
    codeword = db.Column(db.String(128), nullable=False)
    codeword_end_date = db.Column(db.DateTime)
    attached_tasks = db.relationship('AttachedTask', secondary=group_tasks,
                                     backref=db.backref('groups', lazy='dynamic'))


class Task(db.Model, ARMixin):
    id = db.Column(db.Integer, primary_key=True)

    creator_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    creator = db.relationship('User')

    type = db.Column(db.String(128), nullable=False)
    name = db.Column(db.String(128), nullable=False)
    single_solution = db.Column(db.Boolean(), nullable=False, default=False)
    solutions_per_day = db.Column(db.Integer)
    info = db.Column(db.JSON)
    files = association_proxy('task_files', 'file',
                              creator=lambda k, v: TaskFile(description=k, file=v))
    use_peer_review = db.Column(db.Boolean, default=False)
    peer_review_criteria = db.Column(db.JSON)
    peer_review_items = db.Column(db.JSON)


class TaskFile(db.Model, ARMixin):
    id = db.Column(db.Integer, primary_key=True)
    task_id = db.Column(db.Integer, db.ForeignKey('task.id'))
    file_id = db.Column(db.Integer, db.ForeignKey('file_metadata.id'))

    description = db.Column(db.String(128))
    task = db.relationship(Task, backref=db.backref(
        'task_files',
        collection_class=attribute_mapped_collection('description'),
        cascade='all, delete-orphan'))
    file = db.relationship('FileMetadata')


class AttachedTask(db.Model, ARMixin):
    id = db.Column(db.Integer, primary_key=True)

    task = db.relationship('Task', lazy='joined', innerjoin=True)
    task_id = db.Column(db.Integer, db.ForeignKey('task.id'))

    # Attach params
    start_date = db.Column(db.DateTime, nullable=False)
    middle_date = db.Column(db.DateTime)
    finish_date = db.Column(db.DateTime, nullable=False)
    mark_formula = db.Column(db.String(128))
    mark_formula_description = db.Column(db.String(1024))
    hide_results_before_deadline = db.Column(db.Boolean, default=True)
    allow_solutions_after_deadline = db.Column(db.Boolean, default=False)

    # Peer review params
    use_peer_review = db.Column(db.Boolean, default=False)
    peer_review_date = db.Column(db.DateTime)
    min_peer_reviews = db.Column(db.Integer)
    max_peer_reviews = db.Column(db.Integer)
    peer_review_criteria = db.Column(db.JSON)
    peer_review_items = db.Column(db.JSON)


class Solution(db.Model, ARMixin):
    id = db.Column(db.Integer, primary_key=True)
    date = db.Column(db.DateTime, nullable=False, default=datetime.now)
    info = db.Column(db.JSON)

    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    user = db.relationship('User', foreign_keys=[user_id])
    attached_task_id = db.Column(db.Integer, db.ForeignKey('attached_task.id'))
    attached_task = db.relationship('AttachedTask', backref='solutions')
    course_id = db.Column(db.Integer, db.ForeignKey('course.id'))
    course = db.relationship('Course', backref=db.backref(
        'solutions', order_by='Solution.id.desc()'))
    files = association_proxy('solution_files', 'file',
                              creator=lambda k, v: SolutionFile(description=k, file=v))

    rater_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    rater = db.relationship('User', foreign_keys=[rater_id])
    rating_date = db.Column(db.DateTime)

    status = db.Column(db.String(32), nullable=False)
    mark_manual = db.Column(db.Float)
    mark_auto = db.Column(db.Float)
    mark_final = db.Column(db.Float)


class SolutionFile(db.Model, ARMixin):
    id = db.Column(db.Integer, primary_key=True)
    solution_id = db.Column(db.Integer, db.ForeignKey('solution.id'))
    file_id = db.Column(db.Integer, db.ForeignKey('file_metadata.id'))

    description = db.Column(db.String(128))
    solution = db.relationship(Solution, backref=db.backref(
        'solution_files',
        collection_class=attribute_mapped_collection('description'),
        cascade='all, delete-orphan',
        lazy='joined'))
    file = db.relationship('FileMetadata')


class FileMetadata(db.Model, ARMixin):
    id = db.Column(db.Integer, primary_key=True)
    filename = db.Column(db.String(1024), nullable=False)
    path = db.Column(db.String(8192), nullable=False)
    hashsum = db.Column(db.String(1024), nullable=False)


class Image(db.Model, ARMixin):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(1024), nullable=False)
    build_script = db.Column(db.String(1048576), nullable=False)

    creator_id = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=False)
    creator = db.relationship('User')
    date = db.Column(db.DateTime, nullable=False, default=datetime.now)

    status = db.Column(db.String(32), nullable=False)
    build_log = db.Column(db.String(1048576))


class Standings(db.Model, ARMixin):
    id = db.Column(db.Integer, primary_key=True)

    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    user = db.relationship('User')

    course_id = db.Column(db.Integer, db.ForeignKey('course.id'))
    course = db.relationship('Course', backref='standings')

    results = db.Column(db.JSON)


def get_standings_for_course(course_id):
    standings_q = (
        db.session.query(UserInfo, User, Standings)
        .filter(UserInfo.user_id == User.id)
        .filter(UserInfo.course_id == Standings.course_id)
        .filter(User.id == Standings.user_id)
        .filter(Standings.course_id == course_id).all())
    return standings_q


def delete_attached_task(attached_task_id):
    # Выбираем все файлы из таблицы с метадатой, которые прикреплены к solution, но не прикреплены к task_file
    query_files = db.session.execute(
        """
        SELECT id, path FROM file_metadata WHERE id in
        (SELECT file_id FROM solution_file WHERE solution_id in
            (SELECT id FROM solution WHERE attached_task_id = :attached_task_id))
        AND id not in
            (SELECT file_id FROM task_file WHERE file_id in
                (SELECT file_id FROM solution_file WHERE solution_id in
                    (SELECT id FROM solution WHERE attached_task_id = :attached_task_id)))
        AND id not in
            (SELECT file_id FROM solution_file WHERE solution_id not in
                (SELECT id FROM solution WHERE attached_task_id = :attached_task_id));
        """,
        {
            'attached_task_id': attached_task_id
        }
    )
    query_files = query_files.fetchall()
    result_paths = [file[1] for file in query_files]
    result_ids = [file[0] for file in query_files]
    print(f"Deleting files: {result_paths}")
    print(f"Deleting files ids: {result_ids}")

    files_to_restore = db.session.query(FileMetadata).filter(
        FileMetadata.id.in_(
            db.session.query(TaskFile.file_id).filter(
                TaskFile.file_id.in_(
                    db.session.query(SolutionFile.file_id).filter(
                        SolutionFile.solution_id.in_(
                            db.session.query(Solution.id).filter(
                                Solution.attached_task_id == attached_task_id
                            )
                        )
                    )
                )
            )
        )
    ).all()
    # Восстановит в БД такие же файлы как и были прикреплены к задаче и к решению
    print("Files to restore: ", files_to_restore)
    # delete from solution_file cascade automatically with all files metadata
    db.session.execute(
        """
        DELETE FROM solution_file WHERE solution_id in
            (SELECT id FROM solution WHERE attached_task_id = :attached_task_id);
        DELETE FROM solution_review WHERE solution_id in
            (SELECT id FROM solution WHERE attached_task_id = :attached_task_id);
        DELETE FROM solution WHERE attached_task_id = :attached_task_id;
        DELETE FROM group_tasks WHERE attached_task_id = :attached_task_id;
        DELETE FROM attached_task WHERE id = :attached_task_id;
        COMMIT;
        """,
        {
            'attached_task_id': attached_task_id,
        }
    )

    for file_id in result_ids:
        db.session.delete(FileMetadata.query.get(file_id))

    for file in files_to_restore:
        db.session.add(file)
    try:
        db.session.commit()
    except Exception as error:
        print(error)
        db.session.rollback()
        return False

    # due to complex deletion some files may be left in the file system
    # for path in result_paths:
    #     if os.path.exists(path):
    #         try:
    #             os.remove(path)
    #         except Exception as error:
    #             print(error)
    #             continue
    return True


def delete_task(task: Task):
    attached_tasks: tp.List[AttachedTask] = AttachedTask.query.filter_by(task_id=task.id).all()
    for attached_task in attached_tasks:
        if not delete_attached_task(attached_task.id):
            return False
    db.session.commit()
    task_files_metadata: tp.List[FileMetadata] = db.session.query(FileMetadata).filter(
        FileMetadata.id.in_(
            db.session.query(TaskFile.file_id).filter(
                TaskFile.task_id == task.id
            )
        )
    ).all()
    for file_meta in task_files_metadata:
        used_in_other_tasks = FileMetadata.query \
            .join(TaskFile).filter(FileMetadata.id == file_meta.id) \
            .filter(TaskFile.task_id != task.id).first()
        used_in_solutions = SolutionFile.query \
            .filter(SolutionFile.file_id == file_meta.id).first()

        if used_in_other_tasks is None and used_in_solutions is None:
            if os.path.exists(file_meta.path):
                try:
                    os.remove(file_meta.path)
                except Exception as error:
                    print(error)
            db.session.query(TaskFile).filter(
                TaskFile.file_id == file_meta.id
            ).delete()
            db.session.delete(file_meta)
    db.session.query(TaskFile).filter(
        TaskFile.task_id == task.id
    ).delete()
    task.delete()
    return True


def delete_course(course: Course):
    attached_tasks_ids = db.session.execute(
        """
        SELECT attached_task_id FROM group_tasks WHERE group_id in 
            (SELECT id FROM "group" WHERE course_id = :course_id);
        """,
        {
            'course_id': course.id
        }
    ).fetchall()
    attached_tasks_ids = [attached_task_id[0] for attached_task_id in attached_tasks_ids]
    print(attached_tasks_ids)
    print(len(attached_tasks_ids))

    all_attached_tasks = AttachedTask.query.all()
    print(f"all_attached_tasks: {all_attached_tasks}")

    for attached_task_id in attached_tasks_ids:
        if not delete_attached_task(attached_task_id):
            return False
    all_attached_tasks = AttachedTask.query.all()
    print(f"all_attached_tasks after delete: {all_attached_tasks}")
    db.session.execute(
        """
        DELETE FROM group_students WHERE group_id in
            (SELECT id FROM "group" WHERE course_id = :course_id);
        DELETE FROM user_info WHERE course_id = :course_id;
        DELETE FROM standings WHERE course_id = :course_id;
        DELETE FROM user_role WHERE course_id = :course_id;
        DELETE FROM group_tasks WHERE group_id in 
            (SELECT id FROM "group" WHERE course_id = :course_id);
        DELETE FROM "group" WHERE course_id = :course_id;
        DELETE FROM course WHERE id = :course_id;
        COMMIT;
        """,
        {
            'course_id': course.id
        }
    )
    return True


def del_task_file(file_meta: FileMetadata, task_id: int):
    used_in_other_tasks = FileMetadata.query \
        .join(TaskFile).filter(FileMetadata.id == file_meta.id) \
        .filter(TaskFile.task_id != task_id).first()
    used_in_solutions = SolutionFile.query \
        .filter(SolutionFile.file_id == file_meta.id).first()

    if used_in_other_tasks is None and used_in_solutions is None:
        if os.path.exists(file_meta.path):
            try:
                os.remove(file_meta.path)
            except Exception as error:
                print(error)
        file_meta.delete()


def compute_standings(user_id, course_id, attached_task_id):
    def get_solution_mark(order, cond):
        solution = Solution.query \
            .filter_by(user_id=user_id, attached_task_id=attached_task_id) \
            .filter(*cond) \
            .order_by(order).first()
        if solution:
            return solution.mark_auto
        return 0

    params = {}
    date_order = Solution.date.desc()
    mark_order = Solution.mark_final.desc()
    mark_positive = Solution.mark_auto > 0
    attached_task = AttachedTask.query.get(attached_task_id)
    middle_date = attached_task.middle_date
    if middle_date is None:
        params['best_before_middle'] = 0
        middle_date = attached_task.start_date
    else:
        cond = [mark_positive, Solution.date <= attached_task.middle_date]
        params['best_before_middle'] = get_solution_mark(mark_order, cond)

    cond = [mark_positive,
            middle_date < Solution.date,
            Solution.date <= attached_task.finish_date]
    params['best_before_finish'] = get_solution_mark(mark_order, cond)
    params['last_before_finish'] = get_solution_mark(date_order, cond)

    cond = [mark_positive, Solution.date > attached_task.finish_date]
    params['best_after_finish'] = get_solution_mark(mark_order, cond)
    params['last_after_finish'] = get_solution_mark(date_order, cond)

    compute = Interpreter()
    expr = attached_task.mark_formula.format(**params)
    mark_final = ceil(compute(expr) * 10) / 10

    solution = Solution.query \
        .filter_by(user_id=user_id, attached_task_id=attached_task_id) \
        .filter(mark_positive) \
        .order_by(mark_order, date_order).first()
    metric = ''
    if solution:
        metric = solution.info.get('metric', '')

    return mark_final, metric


def init_db():
    db.reflect()
    db.drop_all()
    db.create_all()
    user = User(email='test@example.com', password='12345', firstname='Пользователь',
                lastname='Тестовый', email_confirmed=True, is_admin=True)
    user.save()
