#!/usr/bin/env python3

from database import Image, Solution, Standings, compute_standings
from dateutil.parser import parse
from docker import from_env, APIClient
from glob import glob
from json import loads, dump
from multiprocessing import Process, Queue
from os import cpu_count, environ, getpid, makedirs, remove
from os.path import abspath, basename, exists, getmtime, join
from re import compile, sub
from redis import StrictRedis
from requests import ConnectionError, ReadTimeout
from signal import signal, SIGTERM
from shutil import copy, copytree, move, rmtree
from sqlalchemy.orm.attributes import flag_modified
from subprocess import run
from sys import argv, exit
import tarfile
from zipfile import ZipFile

from app_.bot_helper.bot import send_exception
from app_.status import SolutionStatus
from app_.config import get_settings

settings = get_settings()
TMP_DIR = settings.TMP_DIR


class Master:
    def sigterm_handler(self, signal, frame):
        print('Master got SIGTERM, waiting for workers to stop')
        procs = self.workers + self.builders
        for proc in procs:
            proc.terminate()
        for proc in procs:
            proc.join()
        print('Workers are stopped, stop master')
        exit(0)

    def __init__(self):
        self.msg_queue = None
        signal(SIGTERM, self.sigterm_handler)
        self.workers = []
        self.builders = []
        self.n_workers = max(1, cpu_count() // 2 - 1)
        self.n_builders = 1

    @staticmethod
    def merge_queues(processing, inbox):
        r = StrictRedis(decode_responses=True)

        print(f'Merging and deduping queues `{processing}` and `{inbox}`')
        print(f'Queue `{processing}`:', r.lrange(processing, 0, -1))
        print(f'Queue `{inbox}`:', r.lrange(inbox, 0, -1))

        # Merge processing -> inbox
        proc_ids = r.lrange(processing, 0, -1)
        for proc_id in proc_ids:
            r.rpush(inbox, proc_id)
            r.lpop(processing)

        # Deduplicate inbox
        proc_ids = r.lrange(inbox, 0, -1)
        seen = set()
        for proc_id in proc_ids:
            if proc_id not in seen:
                r.rpush(inbox, proc_id)
                seen.add(proc_id)
            r.lpop(inbox)

        print(f'Queue `{processing}`:', r.lrange(processing, 0, -1))
        print(f'Queue `{inbox}`:', r.lrange(inbox, 0, -1))

    def check_orphans(self):
        self.merge_queues('images_processing', 'images_inbox')
        self.merge_queues('solutions_processing', 'solutions_inbox')

    def run(self):
        self.check_orphans()

        self.msg_queue = Queue()

        n = 0
        for i in range(self.n_workers):
            w = Process(target=Worker(n, self.msg_queue))
            w.start()
            self.workers.append(w)
            n += 1

        for i in range(self.n_builders):
            b = Process(target=ImageBuilder(n, self.msg_queue))
            b.start()
            self.builders.append(b)
            n += 1

        while True:
            proc_id, msg = self.msg_queue.get()
            print('Worker %d: %s' % (proc_id, msg))


def create_dirs(*dirs):
    for d in dirs:
        makedirs(d, exist_ok=True)


class Worker:
    def __init__(self, proc_id, msg_queue):
        self.proc_id = proc_id
        self.msg_queue = msg_queue
        self.exit_process = False
        self.status = 'wait_msg'

        self.tmp_dir = join(TMP_DIR, "worker%02d") % proc_id
        # FixMe: it seems that this rmtree sometimes fails due to directories
        #        owned by root. Figure out, why this happens?
        rmtree(self.tmp_dir, ignore_errors=True)
        try:
            makedirs(self.tmp_dir, exist_ok=False)
        except Exception as err:
            self.send_msg(f"Error when trying to recreate worker dir:\n{err}")

        self.solution_dir_template = join(self.tmp_dir, 'solutions/%04d')
        self.task_data_dir_template = join(self.tmp_dir, 'tasks/%03d')

    def __call__(self):
        try:
            self.run()
        except Exception as e:
            status = f"Unhandled exception in worker. Worker is dead!"
            self.send_msg(status)
            send_exception(
                location=f"{settings.APP_HOST} worker {self.proc_id}",
                status=status,
            )

    def run(self):
        signal(SIGTERM, self.sigterm_handler)
        self.send_msg('Registered sigterm handler')

        r = StrictRedis(decode_responses=True)

        while True:
            if self.exit_process:
                self.send_msg('exiting')
                exit(0)

            self.status = 'wait_msg'
            solution_id = r.brpoplpush('solutions_inbox', 'solutions_processing')
            solution_id = int(solution_id)

            self.status = 'running'
            self.send_msg('testing solution %d' % solution_id)
            solution = Solution.query.get(solution_id)
            if solution is None:
                self.send_msg(f'skipping solution {solution_id}, not found in db')
                r.lrem('solutions_processing', 0, str(solution_id))
                continue
            task = solution.attached_task.task

            solution.status = SolutionStatus.TESTING
            solution.mark_auto = None
            solution.mark_final = None
            solution.info = {}
            flag_modified(solution, 'info')
            solution.save()

            img = Image.query.get(task.info.get('img_id', 'NO image id'))
            if img is None:
                solution.status = SolutionStatus.NO_IMAGE
                solution.save()
                self.send_msg(
                    f'skipping solution {solution_id}, '
                    f'image with id {solution.info.get("img_id", "No solution info...")} not found'
                )
                r.lrem('solutions_processing', 0, str(solution_id))
                continue
            img_name = img.name

            solution_dir = self.solution_dir_template % solution_id
            rmtree(solution_dir, ignore_errors=True)

            # preprocessing
            code_path = join(solution_dir, 'pre_code')
            create_dirs(code_path)
            for f in solution.files.values():
                copy(f.path, join(code_path, f.filename))

            task_dir = self.task_data_dir_template % task.id
            create_dirs(task_dir)

            additional_files_dir = join(task_dir, 'additional_files')
            if exists(additional_files_dir) and \
                getmtime(task.files['additional_files'].path) > getmtime(additional_files_dir):
                rmtree(additional_files_dir)

            if not exists(additional_files_dir) and 'additional_files' in task.files:
                additional_files = task.files.get('additional_files')
                if not additional_files:
                    self.send_msg("No additional FILES. Skipped.")
                    continue
                self.send_msg(f"Extract from {additional_files.path} to {additional_files_dir}")
                ZipFile(additional_files.path).extractall(additional_files_dir)

            if exists(additional_files_dir):
                run(f'cp -r {additional_files_dir}/* {code_path}', shell=True)

            out_path = join(solution_dir, 'pre_output')
            preproc_ok = True
            if 'pre_script' in task.files:
                f = task.files.get('pre_script', None)
                if f is None:
                    self.send_msg('prescript not found')
                    continue
                copy(f.path, join(code_path, f.filename))

                limits = {'memory_limit': '4096m', 'time_limit': 300}
                run_cmd = ['/bin/bash', '-c', 'python3 -B -W ignore pre.py /tmp/output']
                preproc_result = self.run_container(img_name, run_cmd, limits,
                                                    code_path, output_path=out_path)
                self.send_msg(f'preprocessing: {preproc_result}')
                if preproc_result['status'] == 'Ok':
                    rmtree(code_path)
                    out_path = join(out_path, 'output')
                else:
                    preproc_ok = False
                    solution.status = SolutionStatus.PREPROCESSING_FAILED
                    info = {
                        'preproc_status': preproc_result['status'],
                        'preproc_log': preproc_result['stderr']
                    }
                    solution.info = info
                    self.send_msg(f"First: {solution.info=}\n{info=}")
                    flag_modified(solution, 'info')
                    solution.save()
            else:
                move(code_path, out_path)

            if not preproc_ok:
                r.lrem('solutions_processing', 0, str(solution_id))
                continue

            # running on tests
            code_path = join(solution_dir, 'code')
            create_dirs(code_path)
            # get files from preprocessing
            run(f'cp -r {out_path}/* {code_path}', shell=True)
            out_path = join(solution_dir, 'output')

            if 'run_script' in task.files and 'public_tests' in task.files:
                f = task.files['run_script']
                copy(f.path, join(code_path, f.filename))

                public_dir = join(task_dir, 'public')
                private_dir = join(task_dir, 'private')

                if exists(public_dir) and \
                    getmtime(task.files['public_tests'].path) > getmtime(public_dir):
                    rmtree(public_dir)

                if 'private_tests' in task.files and exists(private_dir) and \
                    getmtime(task.files['private_tests'].path) > getmtime(private_dir):
                    rmtree(private_dir)

                if not exists(public_dir):
                    self.send_msg(f"Extract from {task.files['public_tests'].path} to {public_dir}")
                    ZipFile(task.files['public_tests'].path).extractall(public_dir)

                if not exists(private_dir) and 'private_tests' in task.files:
                    self.send_msg(f"Extract from {task.files['private_tests'].path} to {private_dir}")
                    ZipFile(task.files['private_tests'].path).extractall(private_dir)

                results = []
                warmup_cmd = ['/bin/bash', '-c', 'python3 -B -W ignore -c \'print("Ok")\'']
                warmup_limits = {'memory_limit': '512m', 'time_limit': 10}

                run_cmd = ['/bin/bash', '-c', 'python3 -B -W ignore run.py run_single_test /data /tmp/output']
                run_limits = {'memory_limit': str(task.info['memory_limit']) + 'm',
                              'time_limit': task.info['time_limit']}

                check_cmd = ['/bin/bash', '-c', 'python3 -B -W ignore run.py check_test /data /tmp/output']
                check_limits = {'memory_limit': '512m', 'time_limit': 60}

                warmup_attempts = settings.CONTAINER_WARMUP_ATTEMPTS
                for i in range(warmup_attempts):
                    if not i:
                        self.send_msg(f'Warming up container for image {img_name!r}')
                    else:
                        self.send_msg(f'Container warmup timed out! Retrying: {i}')
                    warmup_result = self.run_container(
                        img_name,
                        warmup_cmd,
                        warmup_limits,
                        code_path,
                    )
                    if warmup_result['status'] != 'Time limit':
                        break
                else:
                    if warmup_attempts:
                        status = f"Couldn't warmup the {img_name!r} container"
                        self.send_msg(status)
                        send_exception(
                            location=f"{settings.APP_HOST} worker {self.proc_id}",
                            status=status,
                        )

                testname_regex = compile(r'^[0-9][0-9]_([a-zA-Z0-9]+)_([a-zA-Z0-9_]+)_input$')
                for test_visibility in ['public', 'private']:
                    tests_dir = join(task_dir, test_visibility)
                    input_dirs = sorted(glob(join(tests_dir, '[0-9][0-9]_*_input')))
                    self.send_msg(f'input dirs: {input_dirs}')

                    for i, input_dir in enumerate(input_dirs):
                        self.send_msg(input_dir)
                        m = testname_regex.match(basename(input_dir))
                        if m is None:
                            self.send_msg(f'parsing test filename {basename(input_dir)} failed')
                            continue
                        test_type, test_name = m.groups()

                        out_path = join(solution_dir, 'output/%s/%02d' % (test_visibility, i))
                        create_dirs(out_path)
                        run_result = self.run_container(
                            img_name,
                            run_cmd,
                            run_limits,
                            code_path,
                            output_path=out_path,
                            data_path=input_dir
                        )

                        result = {
                            'time': run_result['time']
                        }

                        if run_result['status'] != 'Ok':
                            result['status'] = run_result['status']
                            result['stderr'] = run_result['stdout'] + run_result['stderr']
                            hidden = False
                        else:
                            if test_type == 'unittest':
                                result['status'] = 'Ok'
                                hidden = test_visibility == 'private'
                            else:
                                gt_path = sub('input$', 'gt', input_dir)
                                copytree(gt_path, join(out_path, 'gt'))
                                check_result = self.run_container(img_name, check_cmd,
                                                            check_limits, code_path,
                                                            data_path=out_path)

                                check_status = check_result['status']
                                if check_status != 'Ok':
                                    result['status'] = 'Check test: %s' % check_status
                                    result['stderr'] = check_result['stderr']
                                    hidden = False
                                else:
                                    result['status'] = check_result['stdout'].rstrip()
                                    hidden = test_visibility == 'private'
                        result['hidden'] = hidden

                        self.send_msg(f"{result['status']=}, {result['time']=}")
                        self.send_msg(f"{result.get('stderr', '')}")

                        results.append(result)
                        solution.info['results'] = results
                        flag_modified(solution, 'info')
                        solution.save()

                if not results:
                    solution.status = SolutionStatus.RUNNING_FAILED
                    solution.info['running_log'] = 'No dirs with tests are found. Make sure they have correct naming'
                    flag_modified(solution, 'info')
                    solution.save()
                    rmtree(solution_dir, ignore_errors=True)
                    r.lrem('solutions_processing', 0, str(solution_id))
                    continue

                grade_cmd = ['/bin/bash', '-c', 'python3 -B -W ignore run.py grade /data /tmp/output']
                grade_path = join(solution_dir, 'grade')
                create_dirs(grade_path)
                with open(join(grade_path, 'results.json'), 'w') as fhandle:
                    dump(results, fhandle)

                grade_result = self.run_container(img_name, grade_cmd,
                                                  check_limits, code_path,
                                                  data_path=grade_path)
                self.send_msg(f"{grade_result=}")
                info = solution.info
                if grade_result['status'] != 'Ok':
                    solution.status = SolutionStatus.GRADING_FAILED
                    info['grading_status'] = grade_result['status']
                    info['grading_log'] = grade_result['stderr']
                else:
                    try:
                        grade_info = loads(grade_result['stdout'])
                    except Exception as err:
                        self.send_msg(f"Error with grading:\n{err}")
                        solution.status = SolutionStatus.GRADING_FAILED
                        info['grading_status'] = 'Failed to parse result'
                        info['grading_log'] = 'Failed to parse json from:\n%s' % grade_result['stdout']
                    else:
                        solution.mark_auto = grade_info['mark']
                        solution.mark_final = grade_info['mark']
                        metric = grade_info['description']
                        info['metric'] = metric
                        solution.status = SolutionStatus.TESTING_COMPLETED
                        solution.info = info
                        self.send_msg(f"Second: {solution.info=}\n{info=}")
                        flag_modified(solution, 'info')
                        solution.save()

                        mark_final, metric = compute_standings(
                            solution.user_id,
                            solution.course_id,
                            solution.attached_task_id)
                        self.send_msg(f'{mark_final=}, {metric=}')
                        update_standings = True
                        standings = Standings.query.filter_by(
                            user_id=solution.user.id,
                            course_id=solution.course.id
                        ).first()

                        if update_standings:
                            self.send_msg('update standings')
                            res = standings.results
                            res[str(solution.attached_task.id)] = mark_final
                            res[str(solution.attached_task.id) + 'metric'] = metric
                            standings.results = res
                            flag_modified(standings, 'results')
                            standings.save()

                solution.info = info
                self.send_msg(f"Third: {solution.info=}\n{info=}")
                flag_modified(solution, 'info')

                solution.save()
            else:
                move(code_path, out_path)

            # TODO: run postprocessing

            rmtree(solution_dir, ignore_errors=True)
            r.lrem('solutions_processing', 0, str(solution_id))

    def run_container(self, image, cmd, limits, code_path,
                      output_path=None, data_path=None):
        cli = from_env(
            # This is the timeout for container CREATION/START only.
            # Raise this value, if workers are dying with
            # requests.exceptions.ReadTimeout exceptions.
            timeout=settings.CONTAINER_CREATE_TIMEOUT,
        )
        volumes = {code_path: {'bind': '/code', 'mode': 'ro'}}
        if data_path is not None:
            volumes[data_path] = {'bind': '/data', 'mode': 'ro'}

        try:
            cont = cli.containers.create(image, cmd, volumes=volumes,
                                         network_disabled=True,
                                         mem_limit=limits['memory_limit'],
                                         memswap_limit=-1,
                                         cpuset_cpus=f'{2 * self.proc_id}',
                                         pids_limit=128)
        except (ReadTimeout, ConnectionError) as err:
            send_exception(
                location=f"{settings.APP_HOST} worker {self.proc_id}",
                status=(
                    "Timeout during container creation in\n"
                    "<code>run_container(\n"
                    f"    {image=!r},\n"
                    f"    {cmd=!r},\n"
                    f"    {limits=!r}),\n"
                    f"    {code_path=!r}),\n"
                    f"    {output_path=!r}),\n"
                    f"    {data_path=!r}),\n"
                    ")</code>"
                ),
            )
            raise

        cont.start()
        api_cli = APIClient()
        result = {}
        time_limit = limits['time_limit']
        timeout = time_limit + settings.CONTAINER_STARTUP_BUDGET
        try:
            exit_code = api_cli.wait(cont.id, timeout=timeout)['StatusCode']
        except Exception as err:
            if not isinstance(
                err,
                (
                    ReadTimeout,
                    # See https://github.com/docker/docker-py/issues/1966
                    ConnectionError,
                ),
            ):
                send_exception(
                    location=f"{settings.APP_HOST} worker {self.proc_id}",
                    status=(
                        "Unexpected Exception during wait in\n"
                        "<code>run_container(\n"
                        f"    {image=!r},\n"
                        f"    {cmd=!r},\n"
                        f"    {limits=!r}),\n"
                        f"    {code_path=!r}),\n"
                        f"    {output_path=!r}),\n"
                        f"    {data_path=!r}),\n"
                        ")</code>"
                    ),
                )
            self.send_msg(f"Error with time limit:\n{err}")
            result['status'] = 'Time limit'
            result['time'] = time_limit
        else:
            result['time'] = self.get_running_time(cont).total_seconds()
            if result['time'] > time_limit:
                result['status'] = 'Time limit'
                result['time'] = time_limit
            elif exit_code != 0:
                cont_info = api_cli.inspect_container(cont.id)
                is_oom_killed = cont_info['State'].get('OOMKilled', False)

                if is_oom_killed:
                    result['status'] = 'Memory limit'
                else:
                    result['status'] = 'Runtime error'
            else:
                result['status'] = 'Ok'
                tar_stream, _ = cont.get_archive('/tmp/output')

                if output_path is not None:
                    makedirs(output_path, exist_ok=True)
                    tar_path = join(self.tmp_dir, 'tmp_tar')

                    with open(tar_path, 'wb') as outfile:
                        for d in tar_stream:
                            outfile.write(d)

                    tarfile.open(tar_path).extractall(path=output_path)
                    remove(tar_path)
        result['stdout'] = cont.logs(stdout=True, stderr=False).decode('utf-8')
        result['stderr'] = cont.logs(stdout=False, stderr=True).decode('utf-8')
        try:
            cont.remove(v=True, force=True)
        except Exception as err:
            self.send_msg(f'Container remove error:\n{err}')
        return result

    def sigterm_handler(self, signal, frame):
        if self.status == 'wait_msg':
            exit(0)
        self.send_msg('will restart after finishing job')
        self.exit_process = True

    def send_msg(self, msg):
        self.msg_queue.put((self.proc_id, msg))

    @staticmethod
    def get_running_time(container):
        api_cli = APIClient()
        cont_info = api_cli.inspect_container(container.id)
        start_time = parse(cont_info['State']['StartedAt'])
        finish_time = parse(cont_info['State']['FinishedAt'])
        return finish_time - start_time


def escape_ansi(line):
    ansi_escape = compile(r'(\x9B|\x1B\[)[0-?]*[ -/]*[@-~]')
    return ansi_escape.sub('', line)


class ImageBuilder:
    def __init__(self, proc_id, msg_queue):
        self.proc_id = proc_id
        self.msg_queue = msg_queue
        self.exit_process = False
        self.status = 'wait_msg'

        self.tmp_dir = join(TMP_DIR, "builder%02d") % proc_id
        # FixMe: it seems that this rmtree sometimes fails due to directories
        #        owned by root. Figure out, why this happens?
        rmtree(self.tmp_dir, ignore_errors=True)
        try:
            makedirs(self.tmp_dir, exist_ok=False)
        except Exception as err:
            self.send_msg(f"Error when trying to recreate builder dir:\n{err}")

    def __call__(self):
        signal(SIGTERM, self.sigterm_handler)
        self.send_msg('registered sigterm handler')

        r = StrictRedis(decode_responses=True)

        while True:
            if self.exit_process:
                self.send_msg('exiting')
                exit(0)

            self.status = 'wait_msg'
            image_id = r.brpoplpush('images_inbox', 'images_processing')

            image = Image.query.get(image_id)
            if image is None:
                self.send_msg('image with id %s isn\'t found, skipping' % image_id)
                r.lrem('images_processing', 0, image_id)
                continue
            image_dir = join(self.tmp_dir, 'images/%02d' % int(image_id))
            create_dirs(image_dir)
            copy('img/Dockerfile', image_dir)
            open(join(image_dir, 'build.sh'), 'w').write(image.build_script)
            image.status = 'Building'
            image.save()

            log = ''
            exit_ok = True
            cli = APIClient()
            for line in cli.build(path=image_dir, tag=image.name,
                                  nocache=True, forcerm=True):
                response = loads(line.decode('utf-8'))
                if 'stream' in response:
                    line = escape_ansi(response['stream'])
                    self.send_msg(f"{line=}")
                    log += line
                    image.build_log = log
                    image.save()
                elif 'error' in response:
                    exit_ok = False
                elif 'aux' in response:
                    pass
                else:
                    self.send_msg(f'Unknown response: {response}')
                    exit_ok = False

            if exit_ok:
                image.status = 'Build ok'
            else:
                image.status = 'Build error'
            self.send_msg(image.status)
            image.save()

            cli.prune_containers()
            cli.prune_images({'dangling': True})
            cli.prune_builds()

            r.lrem('images_processing', 0, image_id)

    def sigterm_handler(self, signal, frame):
        if self.status == 'wait_msg':
            exit(0)
        self.send_msg('will restart after finishing job')
        self.exit_process = True

    def send_msg(self, msg):
        self.msg_queue.put((self.proc_id, msg))


if __name__ == '__main__':
    if len(argv) == 2:
        pidfile = argv[1]
        with open(pidfile, 'w') as fhandle:
            print(getpid(), file=fhandle)
    makedirs(TMP_DIR, exist_ok=True)
    master = Master()
    master.run()
