#!/usr/bin/env python3
import typing as tp

from sqlalchemy import text
from sqlalchemy.orm import aliased

from app_.bot_helper.bot import send_exception
from app_.status import SolutionStatus
from app_.sort_info import SortOrder, SortBy
from decorator import auth_user, templated

from datetime import datetime, date, time
from email.mime.text import MIMEText
from email.utils import formataddr, formatdate
from flask import (
    Flask,
    abort,
    flash,
    g,
    jsonify,
    has_request_context,
    redirect,
    request,
    send_file,
    send_from_directory,
    url_for,
)
from flask.logging import default_handler
import simplejson
from flask_login import (
    LoginManager,
    current_user,
    login_user,
    logout_user,
)
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from flaskext.markdown import Markdown
from forms import *
from hashlib import sha1
from io import BytesIO
from jinja2 import Template
import logging
from os import environ, makedirs, remove
from os.path import join
from random import choice, randint, shuffle
from redis import StrictRedis
from shutil import move, rmtree
from smtplib import SMTP_SSL
from string import ascii_letters
from sqlalchemy.sql import func, or_
from sqlalchemy.orm.attributes import flag_modified
from sys import argv, stdout
from uuid import uuid4
from werkzeug.utils import secure_filename


def request_id():
    if getattr(g, 'request_id', None):
        return g.request_id

    new_uuid = str(uuid4())
    g.request_id = new_uuid

    return new_uuid


class RequestFormatter(logging.Formatter):
    def format(self, record):
        if has_request_context():
            record.url = request.url
            record.remote_addr = request.remote_addr
            record.request_id = request_id()
        else:
            record.url = None
            record.remote_addr = None
            record.request_id = None

        return super().format(record)


formatter = RequestFormatter(
    '[%(request_id)s] %(asctime)s %(levelname)s: %(message)s'
)
default_handler.setFormatter(formatter)

app = Flask(__name__)
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
app.config['SQLALCHEMY_DATABASE_URI'] = APP_SETTINGS.database_uri_sync
app.config['WTF_CSRF_TIME_LIMIT'] = None
app.secret_key = APP_SETTINGS.APP_SECRET_KEY
app.jinja_env.lstrip_blocks = True
app.jinja_env.trim_blocks = True
app.jinja_env.add_extension('jinja2.ext.loopcontrols')
db = SQLAlchemy(app,
                session_options={'autoflush': False},
                engine_options={'json_serializer': simplejson.dumps})
migrate = Migrate(app, db)
Markdown(app)
UPLOAD_DIR = APP_SETTINGS.UPLOAD_DIR
SOLUTIONS_PER_PAGE = 100

app.logger.setLevel(logging.DEBUG)

from database import *

handler = logging.StreamHandler(stdout)
handler.setFormatter(formatter)


@app.errorhandler(500)
def all_exception_handler(e):
    if APP_SETTINGS.APP_HOST == "http://127.0.0.1":
        return e, 500
    exception_url = str(request.path)
    send_exception(
        location=f"{APP_SETTINGS.APP_HOST}{exception_url}",
        status="500 Internal Server Error",
    )


login_manager = LoginManager(app)


@login_manager.user_loader
def load_user(user_id):
    return User.query.get(int(user_id))


@app.after_request
def append_request_id(response):
    response.headers.add('X-Request-Id', request_id())
    return response


@app.before_request
def log_request_info():
    if has_request_context():
        app.logger.debug(f'URL: {request.url}')
        app.logger.debug(f'Form: {request.form}')


@app.route('/')
@auth_user
def index():
    if current_user.courses:
        return redirect(url_for('course',
                                course_id=current_user.courses[-1].id))
    return redirect(url_for('add_user_to_course'))


@app.route('/login', methods=['GET', 'POST'])
@templated()
def login():
    if current_user.is_authenticated:
        return redirect(url_for('index'))
    form = LoginForm()
    if form.validate_on_submit():
        user = User.query.filter_by(email=form.email.data,
                                    password=form.password.data).first()
        if user:
            if user.email_confirmed:
                login_user(user)
                return redirect(url_for('index'))
            flash('Письмо с подтверждением регистрации послано на адрес %s' % user.email,
                  'info')
        else:
            flash('Неправильный логин или пароль', 'warning')
    return dict(form=form)


@app.route('/restore_pass', methods=['GET', 'POST'])
@templated()
def restore_pass():
    if current_user.is_authenticated:
        return redirect(url_for('index'))
    form = RestorePassForm()
    if form.validate_on_submit():
        data = form.email.data.replace("\x00", "\uFFFD")
        user = User.query.filter_by(email=data).first()
        if user:
            try:
                send_pass_email(user.email, user.password)
            except Exception as e:
                app.logger.error(f'Exception while restoring user password: {e}')
            else:
                flash('Письмо с паролем послано на адрес %s' % user.email,
                      'success')
                return redirect(url_for('login'))
            flash('При восстановлении пароля возникла ошибка', 'warning')
        else:
            flash('Такой адрес почты не найден', 'warning')
    return dict(form=form)


FROM_NAME = APP_SETTINGS.FROM_NAME
EMAIL_ADDR = APP_SETTINGS.EMAIL_ADDR
SMTP_ADDR = APP_SETTINGS.SMTP_ADDR
SMTP_PASS = APP_SETTINGS.SMTP_PASS


def send_pass_email(receiver_email, password):
    if not FROM_NAME:
        raise Exception('Email info isn\'t filled')
    body = f"Ваш пароль на сайте {APP_SETTINGS.APP_HOST}:\n\n{password}"

    email = MIMEText(body, 'plain', 'UTF-8')

    subject = f"Восстановление пароля на {APP_SETTINGS.APP_HOST}"
    email['Subject'] = subject

    email['Date'] = formatdate(localtime=True)
    email['From'] = formataddr((FROM_NAME, EMAIL_ADDR))
    email['To'] = receiver_email

    connection = SMTP_SSL(SMTP_ADDR)
    connection.login(EMAIL_ADDR, SMTP_PASS)
    connection.sendmail(EMAIL_ADDR, [receiver_email], email.as_string())
    connection.quit()


def send_confirm_email(receiver_email, confirm_id):
    if not FROM_NAME:
        raise Exception('Email info isn\'t filled')
    body = f"Вы успешно зарегистрировались на сайте {APP_SETTINGS.APP_HOST}.\n\n" \
           f"Для подтверждения email перейдите по ссылке:\n" \
           f"https://{APP_SETTINGS.APP_HOST}/confirm_email/{confirm_id}"

    email = MIMEText(body, 'plain', 'UTF-8')

    subject = f"Регистрация на {APP_SETTINGS.APP_HOST}"
    email['Subject'] = subject

    email['Date'] = formatdate(localtime=True)
    email['From'] = formataddr((FROM_NAME, EMAIL_ADDR))
    email['To'] = receiver_email

    connection = SMTP_SSL(SMTP_ADDR)
    connection.login(EMAIL_ADDR, SMTP_PASS)
    connection.sendmail(EMAIL_ADDR, [receiver_email], email.as_string())
    connection.quit()


@app.route('/logout')
def logout():
    logout_user()
    return redirect(url_for('index'))


@app.route('/register', methods=['GET', 'POST'])
@templated()
def register():
    if current_user.is_authenticated:
        return redirect(url_for('index'))
    form = RegisterForm()
    if form.validate_on_submit():
        user = User()
        form.populate_obj(user)
        user.confirm_id = str(uuid4())
        try:
            user.save()
            send_confirm_email(user.email, user.confirm_id)
        except Exception as e:
            app.logger.error(f'Exception while saving user: {e}')
        else:
            flash('Письмо с подтверждением регистрации послано на адрес %s' % user.email,
                  'success')
            return redirect(url_for('login'))
        flash('При регистрации возникла ошибка', 'warning')
    return dict(form=form)


@app.route('/confirm_email/<confirm_id>')
def confirm_email(confirm_id):
    user = User.query.filter_by(confirm_id=confirm_id).first()
    if user is None:
        abort(404)

    user.confirm_id = ''
    user.email_confirmed = True
    user.save()

    flash('Email успешно подтвержден', 'success')
    return redirect(url_for('login'))


@app.route('/add_course', methods=['GET', 'POST'])
@auth_user
@templated()
def add_course():
    if not current_user.is_admin:
        abort(403)
    form = CourseForm()
    if form.validate_on_submit():
        course = Course()
        form.populate_obj(course)
        suffix = []
        for _ in range(5):
            suffix.append(choice(ascii_letters + '0123456789'))
        # TODO: check codeword is unique
        codeword = course.short_name + '-' + ''.join(suffix)
        group = Group(name='Все студенты', codeword=codeword,
                      codeword_end_date=form.codeword_end_date.data)
        course.groups.append(group)
        course.save()
        flash('Курс успешно добавлен, кодовое слово %s' % codeword, 'success')
        return redirect(url_for('add_course'))
    return dict(form=form, endpoint='add_course')


def create_add_user_to_course_form():
    class F(FlaskForm):
        codeword = StringField('Кодовое слово', [InputRequired('кодовое слово не может быть пустым')])

    if request.method == 'POST':
        group = Group.query.filter_by(codeword=request.form['codeword']).first()
        if group and group.course.user_info_name:
            user_info = UserInfo.query.filter_by(user=current_user, course=group.course).first()
            if user_info is None:
                c = group.course
                F.user_info = StringField(c.user_info_name, [Regexp(c.user_info_regex, message=c.user_info_help)],
                                          description=c.user_info_help)
    return F


@app.route('/add_user_to_course', methods=['GET', 'POST'])
@auth_user
@templated()
def add_user_to_course():
    form = create_add_user_to_course_form()()
    if form.validate_on_submit():
        codeword = form.codeword.data
        group = Group.query.filter_by(codeword=codeword).first()
        if group:
            if group in current_user.groups:
                flash('Вы уже прикреплены к данной группе', 'success')
            else:
                if group.codeword_end_date is not None \
                        and datetime.now() > group.codeword_end_date:
                    flash('Срок действия кодового слова закончился', 'warning')
                else:
                    if hasattr(form, 'user_info'):
                        user_info = UserInfo(info=form.user_info.data,
                                             user=current_user,
                                             course=group.course)
                        user_info.save()
                    if group.course not in current_user.courses:
                        standings = Standings(user=current_user,
                                              course=group.course, results={})
                        standings.save()
                    current_user.groups.append(group)
                    current_user.save()
                    flash('Курс успешно добавлен', 'success')
                    return redirect(url_for('course',
                                            course_id=group.course.id))
        else:
            flash('Курс с кодовым словом %s не найден' % codeword, 'warning')
        return redirect(url_for('add_user_to_course'))
    return dict(form=form, endpoint='add_user_to_course')


@app.route('/edit_course/<int:course_id>', methods=['GET', 'POST'])
@auth_user
@templated('add_course.html')
def edit_course(course_id):
    if not current_user.is_admin:
        abort(403)
    course = Course.query.get_or_404(course_id)
    form = CourseForm(obj=course)
    if form.validate_on_submit():
        form.populate_obj(course)
        course.save()
        flash('Курс успешно отредактирован', 'success')
        return redirect(url_for('course', course_id=course_id))
    return dict(form=form, endpoint='edit_course', course=course)


@app.route('/delete_course/<int:course_id>', methods=['GET', 'POST'])
@auth_user
@templated('delete_course.html')
def delete_course_handler(course_id):
    if not current_user.is_admin:
        abort(403)
    course = Course.query.get_or_404(course_id)
    form = DeleteForm()
    approve = "Я подтверждаю удаление курса"
    returned_dict = dict(
        form=form,
        token=approve,
        endpoint='delete_course',
        course=course,
    )
    if form.validate_on_submit():
        if form.name.data != approve:
            flash('Неверное подтверждение', 'danger')
            return returned_dict
        result = delete_course(course)
        if not result:
            flash('Не получилось удалить курс', 'danger')
            return returned_dict
        flash('Курс успешно удален', 'success')
        return redirect('/')
    return returned_dict


@app.route('/course/<int:course_id>/info')
@auth_user
@templated()
def course(course_id):
    active_course = current_user.get_course_or_404(course_id)
    return dict(active_course=active_course, endpoint='course_info')


@app.route('/course/<int:course_id>/add_group', methods=['GET', 'POST'])
@auth_user
@templated()
def add_group(course_id):
    if not current_user.is_admin:
        abort(403)
    active_course = current_user.get_course_or_404(course_id)
    form = GroupForm()
    if form.validate_on_submit():
        suffix = []
        for i in range(5):
            suffix.append(choice(ascii_letters + '0123456789'))
        codeword = active_course.short_name + '-' + ''.join(suffix)
        group = Group(codeword=codeword)
        form.populate_obj(group)
        active_course.groups.append(group)
        active_course.save()
        flash('Группа успешно добавлена, кодовое слово %s' % codeword, 'success')
        return redirect(url_for('groups', course_id=course_id))
    return dict(form=form, endpoint='add_group')


@app.route('/course/<int:course_id>/edit_group/<int:group_id>', methods=['GET', 'POST'])
@auth_user
@templated('add_group.html')
def edit_group(course_id, group_id):
    if not current_user.is_admin:
        abort(403)
    active_course = current_user.get_course_or_404(course_id)
    if not (1 <= group_id <= len(active_course.groups)):
        abort(404)
    group = active_course.groups[group_id - 1]
    form = GroupForm(obj=group)

    if form.validate_on_submit():
        form.populate_obj(group)
        group.save()
        flash('Группа успешно обновлена', 'success')
        return redirect(url_for('groups', course_id=course_id))
    return dict(form=form, endpoint='edit_group')


@app.route('/course/<int:course_id>/groups')
@auth_user
@templated()
def groups(course_id):
    if not current_user.is_admin:
        abort(403)
    active_course = current_user.get_course_or_404(course_id)
    return dict(course=active_course, endpoint='groups')


@app.route('/course/<int:course_id>/add_role', methods=['GET', 'POST'])
@auth_user
@templated()
def add_role(course_id):
    if not current_user.is_admin:
        abort(403)
    active_course = current_user.get_course_or_404(course_id)
    form = RoleForm()
    form.user_id.choices = [(u.id, '%s %s' % (u.firstname, u.lastname)) for u in active_course.students]
    if form.validate_on_submit():
        user = User.query.get(form.user_id.data)
        role = form.role.data

        user_role = UserRole(user=user, course=active_course, role=role)
        user_role.save()
        flash('Роль успешно добавлена', 'success')
        return redirect(url_for('roles', course_id=course_id))
    return dict(form=form, endpoint='add_role')


@app.route('/course/<int:course_id>/roles')
@auth_user
@templated()
def roles(course_id):
    if not current_user.is_admin:
        abort(403)
    active_course = current_user.get_course_or_404(course_id)
    return dict(course=active_course, endpoint='roles')


def prepare_solution_form(task, solution):
    class SolutionForm(FlaskForm):
        pass

    if task.type == 'test':
        # если будет отредактирован тест так, что количество вопросов уменьшится,
        # то это приведет к list index out of range
        question_indices = solution.info.get('question_indices', None)
        ans_indices = solution.info.get('ans_indices')
        values = solution.info.get('answers')
        if question_indices is None:
            return SolutionForm
        for i, ind in enumerate(question_indices):
            if ind >= len(task.info['questions']):
                continue
            question = task.info['questions'][ind]
            label = question['question']
            key = f'img_{ind:02d}'
            if key in task.files:
                url = url_for('task_file', task_id=task.id, description=key)
                label += f'<br><img src="{url}" width="400px" />'

            setattr(SolutionForm, f'question_{i:02d}',
                    MdTextField(label))
            if question['answers']['type'] == 'number':
                val = None
                if values is not None:
                    val = values[i]
                setattr(SolutionForm, f'question_{i:02d}_answer',
                        DecimalFieldRu('', default=val))
            else:
                if ans_indices:
                    ans_inds = ans_indices[i]
                else:
                    ans_inds = range(len(question['answers']['answers']))

                for j, ans_ind in enumerate(ans_inds):
                    ans = question['answers']['answers'][ans_ind]
                    val = None
                    if values is not None:
                        val = values[i][j]
                    setattr(SolutionForm, f'question_{i:02d}_{j:02d}_answer',
                            BooleanField(ans['ans'], default=val))
    elif task.type in ['programming', 'notebook']:
        for i, solution_file in enumerate(task.info['solution_files']):
            label = 'Файл %s' % solution_file['name']
            size = solution_file['max_size']
            description = 'не больше %d Мб' % size
            setattr(SolutionForm, 'file%d' % i,
                    FileField(label,
                              [InputRequired('необходимо загрузить файл'),
                               FileSize(size * MB, message='максимальный размер файла {size} Мб')],
                              description=description))
    else:
        raise ValueError('Unknown task type "%s"' % task.type)

    return SolutionForm


def update_solution(task, form, solution):
    if task.type == 'test':
        app.logger.debug(f'Update test solution: {form.data} {solution.info}')
        answers = []
        grades = []
        question_indices = solution.info.get('question_indices', None)
        if question_indices is None:
            return solution
        ans_indices = solution.info.get('ans_indices')
        mark = 0
        for i, ind in enumerate(question_indices):
            question = task.info['questions'][ind]
            if question['answers']['type'] == 'number':
                res = form.data[f'question_{i:02d}_answer']
                if abs(float(res) - question['answers']['ans']) <= question['answers']['eps']:
                    grade = True
                    mark += question['mark']
                else:
                    grade = False
            else:
                res = []
                grade = []
                if ans_indices:
                    ans_inds = ans_indices[i]
                else:
                    ans_inds = range(len(question['answers']['answers']))

                for j, ans_ind in enumerate(ans_inds):
                    ans = question['answers']['answers'][ans_ind]
                    cur_res = form.data[f'question_{i:02d}_{j:02d}_answer']
                    res.append(cur_res)
                    grade.append(ans['right'] == cur_res)
                if all(grade):
                    mark += question['mark']
            answers.append(res)
            grades.append(grade)
        solution.info['answers'] = answers
        solution.info['grades'] = grades
        solution.mark_auto = mark
        solution.mark_final = mark
        solution.status = SolutionStatus.SAVED
        flag_modified(solution, 'info')

        standings = Standings.query \
            .filter_by(user_id=solution.user.id,
                       course_id=solution.course.id).first()

        standings.results[str(solution.attached_task.id)] = mark
        flag_modified(standings, 'results')
        standings.save()
        app.logger.debug(f'Updated test solution: {solution.info}')
        solution.save()

    elif task.type in ['programming', 'notebook']:
        for i, solution_file in enumerate(task.info['solution_files']):
            name = 'file%d' % i
            filename = solution_file['name']
            file_meta = save_file(request.files[name], filename)
            solution.files[filename] = file_meta
            app.logger.debug(f'Update solution file {filename}')
    else:
        raise ValueError('Unknown task type "%s"' % task.type)

    solution.date = datetime.now()
    solution.save()
    app.logger.debug(f'Saved solution with id {solution.id}')
    return solution


@app.route('/course/<int:course_id>/task/<int:task_id>', methods=['GET', 'POST'])
@auth_user
@templated('course.html')
def task(course_id, task_id):
    app.logger.debug(f'Logged in as {current_user}')
    active_course = current_user.get_course_or_404(course_id)
    if not (1 <= task_id <= len(active_course.tasks)):
        abort(404)
    active_task = active_course.tasks[task_id - 1]
    active_task.attach_id = task_id

    now = datetime.now()

    max_solutions = active_task.task.solutions_per_day
    max_solutions_string = None
    if max_solutions is not None:
        max_solutions_string = 'Максимальное количество посылок в день — %d %s' % \
                               (max_solutions,
                                get_num_ending(max_solutions, ['штука', 'штуки', 'штук']))

    if request.method == 'POST':
        if now > active_task.finish_date and \
                not active_task.allow_solutions_after_deadline:
            flash('Срок сдачи задания закончился', 'info')
            return redirect(url_for('task', course_id=course_id, task_id=task_id))

        if max_solutions is not None:
            day_start = datetime.combine(date.today(), time())
            n_solutions = Solution.query \
                .filter(Solution.date >= day_start) \
                .filter(Solution.user == current_user) \
                .filter(Solution.attached_task == active_task).count()
            if n_solutions >= max_solutions and not current_user.is_admin:
                flash(max_solutions_string, 'info')
                return redirect(url_for('task', course_id=course_id, task_id=task_id))

    solution = None
    if active_task.task.single_solution:
        solution = Solution.query.filter_by(user=current_user,
                                            attached_task=active_task).first()
    if solution is None and active_task.task.type == 'test':
        # Initialize solution questions
        solution = Solution(user=current_user, attached_task=active_task,
                            course=active_course, status=SolutionStatus.INIT, info={})

        n_questions = len(active_task.task.info['questions'])
        question_sample = list(range(n_questions))
        if active_task.task.info['sample_questions']:
            shuffle(question_sample)
            question_sample = question_sample[:active_task.task.info['n_questions']]
        if active_task.task.info['random_order']:
            shuffle(question_sample)
        solution.info['question_indices'] = question_sample

        if active_task.task.info['random_answer_order']:
            ans_indices = []
            for ind in question_sample:
                question = active_task.task.info['questions'][ind]
                if question['answers']['type'] == 'number':
                    ans_indices.append([])
                else:
                    ans_sample = list(range(len(question['answers']['answers'])))
                    shuffle(ans_sample)
                    ans_indices.append(ans_sample)
            solution.info['ans_indices'] = ans_indices

        solution.save()

    form_cls = prepare_solution_form(active_task.task, solution)
    form = form_cls(data=getattr(solution, 'info', None))
    app.logger.debug(f'Form data: {form.data}')

    if form.validate_on_submit():
        if solution is None:
            solution = Solution(user=current_user, attached_task=active_task,
                                course=active_course,
                                status=SolutionStatus.SAVED, info={})
            app.logger.debug('Created empty solution')

        if active_task.task.type == 'test' and solution.status != SolutionStatus.INIT and not current_user.is_admin and not active_task.task.info.get(
                'many_attempts'):
            flash('Тест можно сдать только один раз', 'error')
            return redirect(url_for('task', course_id=course_id, task_id=task_id))

        solution = update_solution(active_task.task, form, solution)
        solution.save()
        if active_task.task.type == 'programming':
            StrictRedis().lpush('solutions_inbox', solution.id)
            solution.status = SolutionStatus.WAITING
            solution.save()
            app.logger.debug(f'Pushed solution {solution.id}')
        msg = 'Решение отправлено'
        if active_task.use_peer_review:
            standings = Standings.query \
                .filter_by(user_id=solution.user.id, course_id=solution.course.id) \
                .first()

            standings.results[str(solution.attached_task.id) + 'hidden'] = True
            flag_modified(standings, 'results')
            standings.save()
            app.logger.debug(f'Hide standings {standings}')

            solution.info['hidden'] = True
            flag_modified(solution, 'info')
            solution.save()
            app.logger.debug(f'Hide solution {solution}')
            if active_task.task.type == 'notebook':
                solution.status = SolutionStatus.SAVED
                flag_modified(solution, 'status')
                solution.save()
                app.logger.debug('Set solution status to SAVED')

        flash(msg, 'success')
        return redirect(url_for('task', course_id=course_id, task_id=task_id))
    else:
        if form.is_submitted():
            app.logger.debug(f'Form validation failed: {form.errors}')
        else:
            app.logger.debug('Form isn\'t submitted')

    solutions = Solution.query \
        .filter_by(user=current_user, attached_task=active_task) \
        .filter(Solution.status != SolutionStatus.INIT) \
        .order_by(Solution.id.desc()).all()

    solution_reviews = None
    need_at_least_min_n_reviews = None
    reached_max_n_reviews = None

    show_peer_review = bool(solutions)
    if active_task.use_peer_review and active_task.finish_date < now < active_task.peer_review_date:
        solution_reviews = SolutionReview.query \
            .filter_by(rater=current_user) \
            .join(Solution) \
            .filter(Solution.attached_task == active_task) \
            .order_by(SolutionReview.id.asc()).all()
        n_reviews = len(solution_reviews)
        if n_reviews >= active_task.max_peer_reviews:
            reached_max_n_reviews = 'Вы прорецензировали %d %s, это максимальное число рецензий для данного задания.' % \
                                    (n_reviews, get_num_ending(n_reviews, ['решение', 'решения', 'решений']))

        elif n_reviews < active_task.min_peer_reviews:
            n = active_task.min_peer_reviews - n_reviews
            need_at_least_min_n_reviews = 'Осталось прорецензировать минимум %d %s. В противном случае ваше решение не будет оцениваться.' % \
                                          (n, get_num_ending(n, ['решение', 'решения', 'решений']))
    returning_dict = dict(request_id=request_id(), active_course=active_course, active_task=active_task, form=form,
                          solutions=solutions,
                          max_solutions_string=max_solutions_string,
                          solution=solution,
                          solution_reviews=solution_reviews,
                          reached_max_n_reviews=reached_max_n_reviews,
                          need_at_least_min_n_reviews=need_at_least_min_n_reviews,
                          show_peer_review=show_peer_review,
                          endpoint='course_task')

    if active_task.use_peer_review and (
            active_task.min_peer_reviews,
            active_task.max_peer_reviews
    ) == (0, 0) and active_task.finish_date < active_task.peer_review_date < now:
        if solution is None:
            return returning_dict
        sol_id, info = getattr(solution, 'id', None), getattr(solution, 'info', None)
        if sol_id is None or info is None:
            return returning_dict
        info['hidden'] = False
        Solution.query.filter_by(id=sol_id).update({'info': info})
        db.session.commit()
        app.logger.debug(f"Understandable Solution {sol_id} info['hidden'] = False committed")

    return returning_dict


def get_solutions_for_review(current_user, active_task):
    # First check if there are solutions availiable for review for current user
    # solutions that have been reviewed by current user
    rev_solutions_q = db.session.query(Solution.id) \
        .join(SolutionReview) \
        .filter(SolutionReview.rater == current_user) \
        .filter(Solution.attached_task == active_task)

    all_solutions_q = db.session.query(Solution.id) \
        .filter(Solution.user != current_user) \
        .filter(Solution.attached_task == active_task)

    avail_solutions = all_solutions_q.except_(rev_solutions_q).all()

    app.logger.debug(f'Avail solutions: {avail_solutions}')
    if not avail_solutions:
        return []

    # Second, search for solutions that haven't been reviewed at all
    unrev_solutions = Solution.query.join(SolutionReview, isouter=True) \
        .filter(Solution.user != current_user) \
        .filter(Solution.attached_task == active_task) \
        .filter(SolutionReview.id.is_(None)) \
        .all()

    app.logger.debug(f'Unrev solutions: {unrev_solutions}')
    if unrev_solutions:
        app.logger.debug('Found solutions without review')
        return unrev_solutions

    # Third, search for solutions with minimum number of reviews
    # histogram of solution reviews
    hist_solutions_q = db.session.query(Solution, func.count(SolutionReview.id).label('review_count')) \
        .join(SolutionReview) \
        .filter(Solution.user != current_user) \
        .filter(Solution.attached_task == active_task) \
        .group_by(Solution.id) \
        .subquery()

    # min number of reviews per solution
    min_rev_count = db.session.query(func.min(hist_solutions_q.c.review_count))

    # list of solutions with minimum number of reviews that were not already
    # reviewed by current user
    solutions = Solution.query \
        .join(hist_solutions_q, hist_solutions_q.c.id == Solution.id) \
        .filter(hist_solutions_q.c.review_count == min_rev_count.scalar_subquery()) \
        .filter(Solution.id.not_in(rev_solutions_q)) \
        .all()
    if solutions:
        app.logger.debug(f'Found solutions with {min_rev_count.scalar()} reviews')

    return solutions


@app.route('/course/<int:course_id>/task/<int:task_id>/new_peer_review', methods=['GET', 'POST'])
@auth_user
def new_peer_review(course_id, task_id):
    active_course = current_user.get_course_or_404(course_id)
    if not (1 <= task_id <= len(active_course.tasks)):
        abort(404)
    active_task = active_course.tasks[task_id - 1]

    if not active_task.use_peer_review or \
            not (active_task.finish_date < datetime.now() <= active_task.peer_review_date):
        abort(404)

    reviews = SolutionReview.query \
        .filter_by(rater=current_user).join(Solution) \
        .filter(Solution.attached_task == active_task) \
        .order_by(SolutionReview.id.asc()).all()

    n_reviews = len(reviews)

    if reviews and not reviews[-1].is_review_finished:
        return redirect(url_for('peer_review',
                                course_id=course_id,
                                task_id=task_id,
                                peer_review_id=n_reviews))

    if n_reviews >= active_task.max_peer_reviews:
        flash(
            f"Вы прорецензировали {n_reviews} "
            f"{get_num_ending(n_reviews, ['решение', 'решения', 'решений'])}, "
            f"это максимальное число рецензий для данного задания",
            'info')
        return redirect(url_for('task', course_id=course_id, task_id=task_id))

    solutions = get_solutions_for_review(current_user, active_task)
    app.logger.debug(f'Selected solutions for review: {solutions}')

    n_solutions = len(solutions)
    if not n_solutions:
        flash('Нет новых решений на рецензирование', 'info')
        return redirect(url_for('task', course_id=course_id, task_id=task_id))

    solution = solutions[randint(0, n_solutions - 1)]

    app.logger.debug(f'Review solution {solution}')
    solution_review = SolutionReview(rater=current_user, solution=solution)
    solution_review.save()

    review_id = n_reviews + 1

    return redirect(url_for('peer_review',
                            course_id=course_id,
                            task_id=task_id,
                            peer_review_id=review_id))


@app.route('/course/<int:course_id>/admin_review/<int:solution_id>', methods=['GET', 'POST'])
@auth_user
@templated('peer_review.html')
def admin_review(course_id, solution_id):
    if not current_user.is_admin:
        abort(403)

    active_course = current_user.get_course_or_404(course_id)
    solution = Solution.query.get_or_404(solution_id)
    active_task = solution.attached_task

    if not active_task.use_peer_review:
        abort(404)

    solution_review = SolutionReview.query.filter_by(rater=current_user).filter_by(solution=solution).first()
    n_entries = len(active_task.peer_review_items)

    class ReviewForm(FlaskForm):
        review_rules = FieldList(RadioField(coerce=int, validators=[InputRequired('выберите оценку')]),
                                 min_entries=n_entries, max_entries=n_entries)
        rater_comment = TextAreaField('Комментарий', [Optional()])

    form = ReviewForm(obj=solution_review)

    for index_of_question, (item, entry) in enumerate(zip(active_task.peer_review_items, form.review_rules), start=1):
        entry.label.text = f"{index_of_question}. {item['name']}"
        choices = []
        for i, rule in enumerate(item['rules']):
            mark, mark_description = rule['mark'], rule['mark_description']
            ending = get_num_ending(mark, ['балл', 'балла', 'баллов'])
            choices.append((i, f'({mark} {ending}) {mark_description}'))
        entry.choices = choices

    if form.validate_on_submit():
        if not solution_review:
            solution_review = SolutionReview(rater=current_user, solution=solution)
        solution_review.review_rules = form.review_rules.data
        del form.review_rules

        mark = 0
        for chosen_rule, item in zip(solution_review.review_rules, active_task.peer_review_items):
            mark += item['rules'][chosen_rule]['mark']
        mark = round(mark, 2)
        solution_review.mark = mark

        solution_review.is_review_finished = True
        solution_review.date = datetime.now()
        form.populate_obj(solution_review)
        solution_review.save()

        solution_review.solution.mark_final = mark
        solution_review.solution.rater = solution_review.rater
        solution_review.solution.rating_date = solution_review.date
        solution_review.solution.save()

        solution = solution_review.solution
        standings = Standings.query \
            .filter_by(user_id=solution.user.id,
                       course_id=solution.course.id).first()

        res = standings.results
        res[str(solution.attached_task.id)] = mark
        standings.results = res
        flag_modified(standings, 'results')
        standings.save()

        flash('Рецензия сохранена', 'success')
        return redirect(url_for('solutions', course_id=course_id))
    return dict(endpoint='admin_review', form=form, solution=solution, active_course=active_course,
                active_task=solution.attached_task)


@app.route('/course/<int:course_id>/task/<int:task_id>/peer_review/<int:peer_review_id>', methods=['GET', 'POST'])
@auth_user
@templated('peer_review.html')
def peer_review(course_id, task_id, peer_review_id):
    app.logger.debug(f'Logged in as {current_user}')
    active_course = current_user.get_course_or_404(course_id)
    if not (1 <= task_id <= len(active_course.tasks)):
        abort(404)
    active_task = active_course.tasks[task_id - 1]

    if not active_task.use_peer_review or \
            not (active_task.finish_date < datetime.now() <= active_task.peer_review_date):
        abort(404)

    solution_reviews = SolutionReview.query \
        .filter_by(rater=current_user) \
        .join(Solution) \
        .filter(Solution.attached_task == active_task) \
        .order_by(SolutionReview.id.asc()).all()

    if not (1 <= peer_review_id <= len(solution_reviews)):
        abort(404)

    solution_review = solution_reviews[peer_review_id - 1]
    prev_is_review_finished = solution_review.is_review_finished

    n_entries = len(active_task.peer_review_items)

    class ReviewForm(FlaskForm):
        review_rules = FieldList(RadioField(coerce=int, validators=[InputRequired('выберите оценку')]),
                                 min_entries=n_entries, max_entries=n_entries)
        rater_comment = TextAreaField('Комментарий', [Optional()])

    form = ReviewForm(obj=solution_review)

    for index_of_question, (item, entry) in enumerate(zip(active_task.peer_review_items, form.review_rules), start=1):
        entry.label.text = f"{index_of_question}. {item['name']}"
        choices = []
        for i, rule in enumerate(item['rules']):
            mark, mark_description = rule['mark'], rule['mark_description']
            ending = get_num_ending(mark, ['балл', 'балла', 'баллов'])
            choices.append((i, f'({mark} {ending}) {mark_description}'))
        entry.choices = choices

    if form.validate_on_submit():
        solution_review.review_rules = form.review_rules.data
        del form.review_rules

        mark = 0
        for chosen_rule, item in zip(solution_review.review_rules, active_task.peer_review_items):
            mark += item['rules'][chosen_rule]['mark']
        mark = round(mark, 2)
        solution_review.mark = mark

        solution_review.is_review_finished = True
        solution_review.date = datetime.now()
        form.populate_obj(solution_review)
        solution_review.save()

        admin_rated = solution_review.solution.rater_id is not None
        if admin_rated:
            app.logger.debug(
                f'Solution {solution_review.solution} rated by {solution_review.solution.rater}, no recompute')
        else:
            # Recompute mark for solution
            solution_reviews = SolutionReview.query.filter(SolutionReview.solution == solution_review.solution)
            avg_mark = SolutionReview.query.with_entities(func.avg(SolutionReview.mark)).filter(
                SolutionReview.solution == solution_review.solution).all()[0][0]

            avg_mark = round(avg_mark, 2)
            app.logger.debug(f'Recomputed avg mark after review: {solution_review.solution} {avg_mark}')
            solution_review.solution.mark_final = avg_mark
            solution_review.solution.save()

            solution = solution_review.solution
            standings = Standings.query \
                .filter_by(user_id=solution.user.id,
                           course_id=solution.course.id).first()

            res = standings.results
            res[str(solution.attached_task.id)] = avg_mark
            app.logger.debug(f'Update standings {avg_mark}')
            standings.results = res
            flag_modified(standings, 'results')
            standings.save()

        # Unhide current user solution and mark in standings, if needed
        if peer_review_id >= active_task.min_peer_reviews and not prev_is_review_finished:
            solution = Solution.query.filter_by(user_id=current_user.id, attached_task=active_task).first()
            solution.info['hidden'] = False
            flag_modified(solution, 'info')
            solution.save()
            app.logger.debug(f'Unhide solution {solution}')

            standings = Standings.query \
                .filter_by(user_id=current_user.id, course_id=course_id) \
                .first()

            standings.results[str(solution.attached_task.id) + 'hidden'] = False
            flag_modified(standings, 'results')
            standings.save()
            app.logger.debug(f'Unhide standings {standings}')

        flash('Рецензия сохранена', 'success')
        return redirect(url_for('task', course_id=course_id, task_id=task_id))
    return dict(endpoint='peer_review', form=form, active_course=active_course, active_task=active_task,
                solution=solution_review.solution, peer_review_id=peer_review_id, task_id=task_id)


@app.route('/course/<int:course_id>/show_reviews/<int:solution_id>')
@auth_user
@templated('show_reviews.html')
def peer_review_results(course_id, solution_id):
    active_course = current_user.get_course_or_404(course_id)
    solution = Solution.query.get_or_404(solution_id)
    active_task = solution.attached_task

    if not active_task.use_peer_review:
        abort(404)

    if not (current_user.is_admin or datetime.now() > active_task.peer_review_date):
        abort(404)

    if not current_user.is_admin and solution.user != current_user:
        abort(404)

    if not current_user.is_admin and solution.info.get('hidden', False):
        abort(404)

    solution_reviews = SolutionReview.query.filter_by(solution_id=solution_id).order_by(SolutionReview.id.asc())
    return dict(
        endpoint='show_reviews',
        active_course=active_course,
        active_task=active_task,
        solution_reviews=solution_reviews,
        get_num_ending=get_num_ending,
        solution=solution,
        enumerate=enumerate,
        str=str,
    )


@app.route('/course/<int:course_id>/show_grading/<int:solution_id>')
@auth_user
@templated('show_grading.html')
def show_grading(course_id, solution_id):
    active_course = current_user.get_course_or_404(course_id)
    solution = Solution.query.get_or_404(solution_id)
    active_task = solution.attached_task

    if active_task.task.type != 'test':
        abort(404)

    if not (current_user.is_admin or datetime.now() > active_task.finish_date):
        abort(404)

    if not current_user.is_admin and solution.user != current_user:
        abort(404)

    for i in range(len(active_task.task.info['questions'])):
        key = f'img_{i:02d}'
        if key in active_task.task.files:
            url = url_for('task_file', task_id=active_task.task.id, description=key)
            active_task.task.info['questions'][i]['img_url'] = url

    return dict(endpoint='show_grading',
                active_course=active_course,
                active_task=active_task,
                solution=solution,
                zip=zip, get_num_ending=get_num_ending, all=all, len=len)


@app.route('/course/<int:course_id>/task/<int:task_id>/rerun_solutions')
@auth_user
def rerun_solutions(course_id, task_id):
    if not current_user.is_admin:
        abort(403)
    active_course = current_user.get_course_or_404(course_id)
    if not (1 <= task_id <= len(active_course.tasks)):
        abort(404)
    active_task = active_course.tasks[task_id - 1]
    solutions = Solution.query \
        .filter_by(attached_task_id=active_task.id) \
        .order_by(Solution.id.asc()).all()
    for solution in solutions:
        StrictRedis().lpush('solutions_inbox', solution.id)
        app.logger.debug(f'Pushed {solution.id}')
    flash('Все решения добавлены в очередь', 'success')
    return redirect(url_for('task', course_id=course_id, task_id=task_id))


def get_num_ending(num, endings):
    num = num % 100
    if 11 <= num <= 19:
        return endings[2]
    if num < 1:
        return endings[2]
    if num == 1:
        return endings[0]
    if num < 5:
        return endings[1]
    return endings[2]


def create_solution_grade_form(solution):
    class F(FlaskForm):
        pass

    endings = ['балл', 'балла', 'баллов']

    questions = solution.attached_task.task.info['questions']
    n_questions = len(questions)
    criteria = solution.info.get('criteria', None) or ([None] * n_questions)
    for i, question in enumerate(questions):
        q = question['question']
        setattr(F, 'question%d' % i,
                TextAreaField('Вопрос',
                              render_kw={'disabled': '', 'placeholder': q}))

        student_answer = solution.info['question%d' % i]
        setattr(F, 'answer%d' % i,
                TextAreaField('Ответ студента',
                              render_kw={'disabled': '', 'placeholder': student_answer}))

        correct_answer = question['correct_answer']
        setattr(F, 'correct_answer%d' % i,
                TextAreaField('Правильный ответ',
                              render_kw={'disabled': '', 'placeholder': correct_answer}))

        choices = []
        for j, crit in enumerate(question['criteria']):
            word = get_num_ending(crit['mark'], endings)
            desc = '(%.2f %s) %s' % (crit['mark'], word, crit['description'])
            choices.append((j, desc))
        setattr(F, 'criterion%d' % i,
                RadioField('Оценка', choices=choices, default=criteria[i], coerce=int))
    return F


@app.route('/grade_solution/<int:solution_id>', methods=['GET', 'POST'])
@auth_user
def grade_solution(solution_id):
    if not current_user.is_admin:
        abort(403)

    solution = Solution.query.get_or_404(solution_id)
    if solution.attached_task.task.type == 'programming':
        StrictRedis().lpush('solutions_inbox', solution.id)
        app.logger.debug(f'Pushed {solution.id}')
        flash(f"Решение №{solution.id} добавлено в очередь", 'success')
        return redirect(url_for('solutions',
                                course_id=solution.course.id))

    abort(404)


@app.route('/add_task/test', methods=['GET', 'POST'])
@auth_user
@templated()
def add_task():
    if not current_user.is_admin:
        abort(403)
    form = TaskForm()
    if form.validate_on_submit():
        task = Task(type='test', creator=current_user,
                    name=form.name.data, single_solution=True)

        for i, question in enumerate(form.info.questions):
            field = question.img
            if request.files[field.name].filename != '':
                app.logger.debug(f'Save img {field.name}')
                key = f'img_{i:02d}'
                sec_filename = secure_filename(request.files[field.name].filename)
                task.files[key] = save_file(request.files[field.name], sec_filename)

        res_info = form.info.data
        for i in range(len(res_info['questions'])):
            del res_info['questions'][i]['img']

        task.info = res_info
        task.save()

        flash('Задание успешно добавлено', 'success')
        return redirect(url_for('add_task'))
    return dict(endpoint='add_task', form=form)


def save_file(file_obj, filename):
    makedirs(UPLOAD_DIR, exist_ok=True)

    tmp_path = join(UPLOAD_DIR, 'tmp_' + uuid4().hex)
    file_obj.save(tmp_path)

    with open(tmp_path, 'rb') as f:
        file_hash = sha1()
        while chunk := f.read(8192):
            file_hash.update(chunk)
    hashsum = file_hash.hexdigest()

    path = join(UPLOAD_DIR, hashsum)
    move(tmp_path, path)

    file_meta = FileMetadata.query.filter_by(filename=filename, hashsum=hashsum).first()
    if file_meta is None:
        file_meta = FileMetadata(filename=filename, hashsum=hashsum, path=path)
        app.logger.debug(f'Saving {filename} with hash {hashsum}')
    else:
        app.logger.debug(f'File {filename} with hash {hashsum} already exists')

    return file_meta


@app.route('/add_task/programming', methods=['GET', 'POST'])
@auth_user
@templated()
def add_task_programming():
    if not current_user.is_admin:
        abort(403)
    form = ProgrammingTaskForm()
    images = Image.query.all()
    form.img_id.choices = [(img.id, '%s' % img.name) for img in images]
    if form.validate_on_submit():
        info = {'intro_description': form.intro_description.data,
                'time_limit': form.time_limit.data,
                'memory_limit': form.memory_limit.data,
                'img_id': form.img_id.data,
                'solution_files': form.solution_files.data}
        task = Task(type='programming', name=form.name.data, info=info,
                    creator=current_user,
                    solutions_per_day=form.solutions_per_day.data)
        for field in form:
            if field.type == 'FileField' and request.files[field.name].filename != '':
                filename = form.default_filenames[field.name]
                task.files[field.name] = save_file(request.files[field.name], filename)
        task.save()
        flash('Задание успешно добавлено', 'success')
        return redirect(url_for('add_task_programming'))

    return dict(endpoint='add_task_programming', form=form)


@app.route('/add_task/notebook', methods=['GET', 'POST'])
@auth_user
@templated()
def add_task_notebook():
    if not current_user.is_admin:
        abort(403)
    form = NotebookTaskForm()
    if form.validate_on_submit():
        info = {
            'intro_description': form.intro_description.data,
            'solution_files': form.solution_files.data
        }
        task = Task(type='notebook', name=form.name.data, info=info,
                    use_peer_review=form.use_peer_review.data,
                    peer_review_items=form.peer_review_items.data,
                    peer_review_criteria=form.peer_review_criteria.data,
                    creator=current_user, single_solution=True)
        for field in form:
            if field.type == 'FileField' and request.files[field.name].filename != '':
                filename = form.default_filenames[field.name]
                task.files[field.name] = save_file(request.files[field.name], filename)
        task.save()
        flash('Задание успешно добавлено', 'success')
        return redirect(url_for('add_task_notebook'))
    return dict(endpoint='add_task_notebook', form=form)


def clear_validators(*fields):
    for field in fields:
        if field.validators:
            del field.validators[:]


def create_task_form(task):
    if task.type == 'test':
        data = {'name': task.name,
                'info': task.info}
        form = TaskForm(data=data)
        for file_description in task.files:
            url = url_for('task_file', task_id=task.id, description=file_description)
            file_id = int(file_description.split('_')[-1])
            form.info.questions[file_id].img.label.text += ' (<a href=\'%s\'>загруженное</a>)' % url
    elif task.type == 'programming':
        data = {'name': task.name,
                'intro_description': task.info['intro_description'] if 'intro_description' in task.info else '',
                'solutions_per_day': task.solutions_per_day,
                'time_limit': task.info['time_limit'],
                'memory_limit': task.info['memory_limit'],
                'img_id': task.info['img_id'],
                'solution_files': task.info['solution_files']}
        form = ProgrammingTaskForm(data=data)
        images = Image.query.all()
        form.img_id.choices = [(img.id, '%s' % img.name) for img in images]
        clear_validators(form.task_description,
                         form.public_tests,
                         form.run_script)
        for file_description in task.files:
            url = url_for('task_file', task_id=task.id, description=file_description)
            form[file_description].label.text += ' (<a href=\'%s\'>загруженный</a>)' % url
    elif task.type == 'notebook':
        data = {'name': task.name,
                'intro_description': task.info['intro_description'] if 'intro_description' in task.info else '',
                'solution_files': task.info['solution_files'],
                'use_peer_review': task.use_peer_review,
                'peer_review_criteria': task.peer_review_criteria,
                'peer_review_items': task.peer_review_items
                }
        form = NotebookTaskForm(data=data)
        for file_description in task.files:
            url = url_for('task_file', task_id=task.id, description=file_description)
            form[file_description].label.text += ' (<a href=\'%s\'>загруженный</a>)' % url
    else:
        raise ValueError('Task type "%s" isn\'t supported' % task.type)
    return form


def update_task(task, form):
    if task.type == 'test':
        task.name = form.name.data

        for i, question in enumerate(form.info.questions):
            field = question.img
            if request.files[field.name].filename != '':
                app.logger.debug(f'Save img {field.name}')
                key = f'img_{i:02d}'
                sec_filename = secure_filename(request.files[field.name].filename)

                old_file = task.files.get(key)
                new_file = save_file(request.files[field.name], sec_filename)

                task.files[key] = new_file
                task.save()
                if old_file and old_file.hashsum != new_file.hashsum:
                    del_task_file(old_file, task.id)

        res_info = form.info.data
        for i in range(len(res_info['questions'])):
            del res_info['questions'][i]['img']

        task.info = res_info
        task.save()
    elif task.type == 'programming':
        info = {'intro_description': form.intro_description.data,
                'time_limit': form.time_limit.data,
                'memory_limit': form.memory_limit.data,
                'img_id': form.img_id.data,
                'solution_files': form.solution_files.data}
        task.name = form.name.data
        task.info = info
        task.solutions_per_day = form.solutions_per_day.data
        task.save()
        for field in form:
            if field.type == 'FileField' and request.files[field.name].filename != '':
                filename = form.default_filenames[field.name]
                old_file = task.files.get(field.name)
                new_file = save_file(request.files[field.name], filename)

                task.files[field.name] = new_file
                task.save()

                if old_file and old_file.hashsum != new_file.hashsum:
                    del_task_file(old_file, task.id)

    elif task.type == 'notebook':
        info = {
            'intro_description': form.intro_description.data,
            'solution_files': form.solution_files.data
        }
        task.name = form.name.data
        task.info = info
        task.use_peer_review = form.use_peer_review.data
        task.peer_review_items = form.peer_review_items.data
        task.peer_review_criteria = form.peer_review_criteria.data
        task.save()
        for field in form:
            if field.type == 'FileField' and request.files[field.name].filename != '':
                filename = form.default_filenames[field.name]
                old_file = task.files.get(field.name)
                new_file = save_file(request.files[field.name], filename)
                task.files[field.name] = new_file
                task.save()
                if old_file and old_file.hashsum != new_file.hashsum:
                    del_task_file(old_file, task.id)
    else:
        raise ValueError('Task type "%s" isn\'t supported' % task.type)
    return task


@app.route('/edit_task/<int:task_id>', methods=['GET', 'POST'])
@auth_user
@templated()
def edit_task(task_id):
    if not current_user.is_admin:
        abort(403)
    templates = {'test': 'add_task.html',
                 'programming': 'add_task_programming.html',
                 'notebook': 'add_task_notebook.html'}
    endpoints = {'test': 'edit_task',
                 'programming': 'edit_task_programming',
                 'notebook': 'edit_task_notebook'}
    task = Task.query.get_or_404(task_id)
    form = create_task_form(task)
    if form.validate_on_submit():
        task = update_task(task, form)
        flash('Задание успешно обновлено', 'success')
        return redirect(url_for('edit_task', task_id=task_id))

    return dict(form=form, endpoint=endpoints[task.type],
                template=templates[task.type], task_id=task_id)


@app.route('/delete_task/<int:task_id>', methods=['GET', 'POST'])
@auth_user
@templated('delete_task.html')
def delete_task_handler(task_id):
    if not current_user.is_admin:
        abort(403)
    task = Task.query.get_or_404(task_id)
    form = DeleteForm()
    attached_tasks = AttachedTask.query.filter_by(task_id=task_id).all()
    if len(attached_tasks) > 0:
        approve = f"Я подтверждаю удаление задания c прикреплениями - {len(attached_tasks)}"
    else:
        approve = "Я подтверждаю удаление задания без прикреплений"
    returned_dict = dict(
        form=form,
        token=approve,
        endpoint='delete_task',
    )
    print(task.id)
    if form.validate_on_submit():
        if form.name.data != approve:
            flash('Неверное подтверждение', 'danger')
            return returned_dict
        result = delete_task(task)
        if not result:
            flash('Не удалось удалить задание', 'danger')
            return redirect('/')
        flash("Задание успешно удалено", 'success')
        return redirect('/')
    return returned_dict


@app.route('/attach_task', methods=['GET', 'POST'])
@auth_user
@templated()
def attach_task():
    if not current_user.is_admin:
        abort(403)
    form = AttachTaskForm()
    form.group_id.choices = [(g.id, '%s → %s' % (g.course.name, g.name)) for g in current_user.groups]
    form.task_id.choices = [(t.id, t.name) for t in Task.query.all()]
    if form.validate_on_submit():
        attached_task = AttachedTask()
        # Get data from Task table and insert into attached task
        task_b = Task.query.get_or_404(form.task_id.data)
        attached_task.use_peer_review = task_b.use_peer_review
        attached_task.peer_review_items = task_b.peer_review_items
        attached_task.peer_review_criteria = task_b.peer_review_criteria
        del form.peer_review_items
        del form.peer_review_criteria
        del form.use_peer_review
        form.populate_obj(attached_task)
        attached_task.save()

        group_id = form.group_id.data
        group = Group.query.get(group_id)
        group.attached_tasks.append(attached_task)
        group.save()

        app.logger.debug(f'Attach task {attached_task.id} with groups {form.group_id.data}')

        flash('Задание успешно прикреплено', 'success')
        return redirect(url_for('attach_task'))
    return dict(form=form, endpoint='attach_task')


@app.route('/edit_attached_task/course/<int:course_id>/task/<int:task_id>',
           methods=['GET', 'POST'])
@auth_user
@templated('edit_attached_task.html')
def edit_attached_task(course_id, task_id):
    if not current_user.is_admin:
        abort(403)
    active_course = current_user.get_course_or_404(course_id)
    if not (1 <= task_id <= len(active_course.tasks)):
        abort(404)
    active_task = active_course.tasks[task_id - 1]
    form = AttachTaskForm(obj=active_task)

    form.group_id.choices = [(g.id, '%s → %s' % (active_course.name, g.name)) for g in active_task.groups]
    clear_validators(form.group_id)

    form.task_id.choices = [(t.id, t.name) for t in Task.query.all()]
    form.task_id.default = active_task.task.id

    if form.validate_on_submit():
        active_task.peer_review_items = form.peer_review_items.data
        del form.peer_review_items

        form.populate_obj(active_task)
        active_task.save()
        flash('Прикрепление успешно обновлено', 'success')
        return redirect(url_for('task', course_id=course_id, task_id=task_id))

    return dict(
        form=form,
        endpoint='edit_attached_task',
        active_course_id=course_id,
        task_id=task_id
    )


@app.route('/delete_attached_task/course/<int:course_id>/task/<int:task_id>',
           methods=['GET', 'POST'])
@auth_user
@templated('delete_attached_task.html')
def delete_attach_task_handler(course_id, task_id):
    if not current_user.is_admin:
        abort(403)
    active_course = current_user.get_course_or_404(course_id)
    if not (1 <= task_id <= len(active_course.tasks)):
        abort(404)
    active_task = active_course.tasks[task_id - 1]
    form = DeleteForm()
    approve = "Я подтверждаю удаление прикрепления"
    returned_dict = dict(form=form,
                         endpoint='delete_attached_task',
                         active_course_id=course_id,
                         active_course_name=active_course.name,
                         token=approve,
                         task_id=task_id)
    print(active_task.id)
    if form.validate_on_submit():
        if form.name.data != approve:
            flash('Неверное подтверждение', 'danger')
            return returned_dict
        result = delete_attached_task(active_task.id)
        if not result:
            flash('Не получилось удалить прикрепленное задание', 'danger')
        else:
            flash(
                f'Прикрепленное задание '
                f'успешно удалено из курса - {active_course.name}',
                'success')
        return redirect(url_for('course', course_id=course_id))
    return returned_dict


@app.route('/course/<int:course_id>/standings')
@auth_user
@templated()
def standings(course_id):
    active_course = current_user.get_course_or_404(course_id)
    standings_q = get_standings_for_course(course_id)
    return dict(active_course=active_course, standings_q=standings_q,
                endpoint='standings')


@app.route('/course/<int:course_id>/standings_csv')
@auth_user
def standings_csv(course_id):
    if not current_user.is_admin:
        abort(403)
    active_course = current_user.get_course_or_404(course_id)
    standings = Standings.query.filter_by(course_id=course_id).all()
    data = dict(active_course=active_course, standings=standings,
                now=datetime.now(), endpoint='standings',
                user_info_cls=UserInfo)
    template = Template(open('templates/csv_gen/standings.csv').read())
    csv_content = template.render(data)
    return send_file(BytesIO(csv_content.encode('utf-8')), attachment_filename='standings.txt')


@app.route('/course/<int:course_id>/solutions')
@app.route('/course/<int:course_id>/solutions/page/<int:page_id>')
@auth_user
@templated()
def solutions(course_id, page_id=None):
    if not current_user.is_admin:
        abort(403)
    if page_id is None:
        page_id = 1

    sort_order = request.args.get('sort_order') or 'desc'
    sort_by = request.args.get('sort_by') or 'id'

    sort_order_dict = {
        SortBy.ID: Solution.id,
        SortBy.DATE: Solution.date,
        SortBy.MARK: Solution.mark_final,
    }
    if not sort_order_dict.get(sort_by):
        abort(404)
    if sort_order not in [item.value for item in SortOrder]:
        abort(404)

    user_id = request.args.get('user_id')
    user_firstlastname = None
    if user_id:
        user = User.query.filter_by(id=user_id).first_or_404()
        user_firstlastname = user.lastname + ' ' + user.firstname

    status = request.args.get('status')
    available_status = [item.value for item in SolutionStatus]
    if status and status not in available_status:
        abort(404)

    active_course = current_user.get_course_or_404(course_id)

    task_id = request.args.get('task_id')
    attach_task_id = None
    if task_id:
        if not task_id.isdigit():
            abort(404)
        if not 1 <= int(task_id) <= len(active_course.tasks):
            abort(404)
        attach_task_id = active_course.tasks[int(task_id) - 1].id

    solution_q = (Solution.query
                  .options(joinedload(Solution.user, innerjoin=True))
                  .filter_by(course_id=active_course.id))

    if user_id:
        solution_q = solution_q.filter_by(user_id=user_id)
    if attach_task_id:
        solution_q = solution_q.filter_by(attached_task_id=attach_task_id)
    if status:
        solution_q = solution_q.filter_by(status=status)
    else:
        solution_q = solution_q.filter(Solution.status != SolutionStatus.INIT)

    order_by = sort_order_dict[sort_by].desc() if sort_order == SortOrder.DESC else sort_order_dict[sort_by].asc()

    solution_page = (solution_q.order_by(order_by)
                     .paginate(page_id, SOLUTIONS_PER_PAGE))
    students = (db.session.query(User, UserInfo)
                .join(Group.students).join(Course)
                .filter(UserInfo.user_id == User.id)
                .filter(UserInfo.course_id == course_id)
                .filter(Course.id == course_id)
                .order_by(User.id).all())

    return dict(active_course=active_course, solution_page=solution_page,
                user_id=user_id, task_id=task_id, status=status, user_firstlastname=user_firstlastname,
                sort_order=sort_order, sort_by=sort_by, endpoint='solutions', students=students,
                available_status=available_status)


@app.route('/course/<int:course_id>/task/<int:task_id>/show_reviews')
@app.route(
    '/course/<int:course_id>/task/<int:task_id>/show_reviews/page/<int:page_id>'
)
@auth_user
@templated('show_task_reviews.html')
def show_task_reviews(
        course_id: int, task_id: int, page_id: tp.Optional[int] = None
):
    if not current_user.is_admin:
        abort(403)
    if page_id is None:
        page_id = 1

    sort_order = request.args.get('sort_order') or 'desc'
    sort_by = request.args.get('sort_by') or 'id'
    sort_order_dict = {
        SortBy.ID: SolutionReview.id,
        SortBy.DATE: SolutionReview.date,
        SortBy.MARK: SolutionReview.mark,
    }
    if not sort_order_dict.get(sort_by):
        abort(404)
    if sort_order not in [item.value for item in SortOrder]:
        abort(404)

    user_id = request.args.get('user_id')
    user_firstlastname = None
    if user_id:
        user = User.query.filter_by(id=user_id).first_or_404()
        user_firstlastname = user.lastname + ' ' + user.firstname

    rater_id = request.args.get('rater_id')
    rater_firstlastname = None
    if rater_id:
        rater = User.query.filter_by(id=rater_id).first_or_404()
        rater_firstlastname = rater.lastname + ' ' + rater.firstname

    active_course = current_user.get_course_or_404(course_id)
    active_task = active_course.tasks[task_id - 1]

    user_solution = aliased(User)
    user_rater = aliased(User)
    reviews_q = (
        SolutionReview.query.join(Solution)
        .filter_by(course_id=active_course.id, attached_task_id=active_task.id)
        .join(user_solution, Solution.user)
        .join(user_rater, SolutionReview.rater)
    )

    if user_id:
        reviews_q = reviews_q.filter(user_solution.id == user_id)
    if rater_id:
        reviews_q = reviews_q.filter(user_rater.id == rater_id)
    # TODO: review not finished

    order_by = (
        sort_order_dict[sort_by].desc()
        if sort_order == SortOrder.DESC
        else sort_order_dict[sort_by].asc()
    )

    reviews_page = reviews_q.order_by(order_by).paginate(page_id, SOLUTIONS_PER_PAGE)
    students = (
        db.session.query(User, UserInfo)
        .join(Group.students)
        .join(Course)
        .filter(UserInfo.user_id == User.id)
        .filter(UserInfo.course_id == course_id)
        .filter(Course.id == course_id)
        .order_by(User.id)
        .all()
    )

    return dict(
        active_course=active_course,
        reviews_page=reviews_page,
        task_id=task_id,
        user_id=user_id,
        rater_id=rater_id,
        user_firstlastname=user_firstlastname,
        rater_firstlastname=rater_firstlastname,
        sort_order=sort_order,
        sort_by=sort_by,
        endpoint='show_task_reviews',
        students=students,
    )


@app.route('/course/<int:course_id>/task/<int:task_id>/show_reviews_stat')
@app.route(
    '/course/<int:course_id>/task/<int:task_id>/show_reviews_stat/page/<int:page_id>'
)
@auth_user
@templated('show_reviews_stat.html')
def show_reviews_stat(
        course_id: int, task_id: int, page_id: tp.Optional[int] = None
):
    if not current_user.is_admin:
        abort(403)
    if page_id is None:
        page_id = 1

    sort_order = request.args.get('sort_order') or 'desc'
    sort_by = request.args.get('sort_by') or 'id'
    sort_order_dict = {
        SortBy.ID: User.id,
        SortBy.MARK: 'avg_reviews_mark',
        SortBy.REVIEWS_COUNT: 'reviews_count',
    }
    if not sort_order_dict.get(sort_by):
        abort(404)
    if sort_order not in [item.value for item in SortOrder]:
        abort(404)

    rater_id = request.args.get('rater_id')
    rater_firstlastname = None
    if rater_id:
        rater = User.query.filter_by(id=rater_id).first_or_404()
        rater_firstlastname = rater.lastname + ' ' + rater.firstname

    active_course = current_user.get_course_or_404(course_id)
    active_task = active_course.tasks[task_id - 1]

    raters_q = (
        db.session.query(User,
                         func.avg(SolutionReview.mark).label('avg_reviews_mark'),
                         func.count(SolutionReview.id).label('reviews_count'))
        .join(SolutionReview)
        .join(Solution)
        .filter_by(course_id=active_course.id, attached_task_id=active_task.id)
        .group_by(User.id)
    )

    if rater_id:
        raters_q = raters_q.filter(User.id == rater_id)

    if isinstance(sort_order_dict[sort_by], str):
        order_by = text(f'{sort_order_dict[sort_by]} {sort_order}')
    else:
        order_by = (
            sort_order_dict[sort_by].desc()
            if sort_order == SortOrder.DESC
            else sort_order_dict[sort_by].asc()
        )

    raters_page = raters_q.order_by(order_by).paginate(page_id, SOLUTIONS_PER_PAGE)
    students = (
        db.session.query(User, UserInfo)
        .join(Group.students)
        .join(Course)
        .filter(UserInfo.user_id == User.id)
        .filter(UserInfo.course_id == course_id)
        .filter(Course.id == course_id)
        .order_by(User.id)
        .all()
    )

    return dict(
        active_course=active_course,
        raters_page=raters_page,
        task_id=task_id,
        rater_id=rater_id,
        rater_firstlastname=rater_firstlastname,
        sort_order=sort_order,
        sort_by=sort_by,
        endpoint='show_reviews_stat',
        students=students,
        round=round,
    )


@app.route('/course/<int:course_id>/task/<int:task_id>/generate_reviews_csv')
@auth_user
def generate_reviews_csv(course_id: int, task_id: int):
    if not current_user.is_admin:
        abort(403)
    active_course = current_user.get_course_or_404(course_id)
    active_task = active_course.tasks[task_id - 1]
    if not active_task.use_peer_review:
        abort(404)
    solutions = Solution.query.filter_by(course_id=active_course.id, attached_task_id=active_task.id).all()
    reviews: tp.Dict[Solution, tp.List[SolutionReview]] = {}
    users: tp.Dict[User, tp.List[User]] = {}
    urls_for_solution_files: tp.Dict[Solution, str] = {}
    for solution in solutions:
        tmp: tp.List[SolutionReview] = SolutionReview.query.filter_by(solution_id=solution.id).all()
        reviews[solution] = tmp
        users[solution.user] = list(map(lambda x: x.rater, tmp))
        # ('/solution_file/<int:solution_id>/<description>')
        urls_for_solution_files[solution] = url_for('solution_file', solution_id=solution.id,
                                                    description=next(iter(solution.solution_files)))
    if len(users) != len(reviews):
        flash('Количество рецензируемых не равно количеству рецензий', 'danger')
        return redirect(url_for('task', course_id=course_id, task_id=task_id))

    data = dict(
        urls=urls_for_solution_files,
        reviews=reviews,
        users=users,
        zip=zip,
        APP_HOST=APP_SETTINGS.APP_HOST,
    )
    template = Template(open('templates/csv_gen/generate_reviews.csv').read())
    csv_content = template.render(data)
    return send_file(BytesIO(csv_content.encode('utf-8')), attachment_filename='generated_reviews.csv')


@app.route('/course/<int:course_id>/solutions/generate_solutions_csv')
@auth_user
def generate_solutions_csv(course_id: int):
    if not current_user.is_admin:
        abort(403)
    active_course = current_user.get_course_or_404(course_id)
    query_res = db.session.query(
        User.id,  # 0
        User.email,  # 1
        User.lastname,  # 2
        User.firstname,  # 3
        User.patronym,  # 4
        Solution.id,  # 5
        Solution.date,  # 6
        Solution.user_id,  # 7
        Solution.attached_task_id,  # 8
        Solution.status,  # 9
        Solution.mark_auto,  # 10
        Solution.mark_final,  # 11
        FileMetadata.filename,  # 12
        FileMetadata.path,  # 13
        FileMetadata.hashsum,  # 14
    ).filter(
        Solution.course_id == active_course.id,
        User.id == Solution.user_id,
        SolutionFile.solution_id == Solution.id,
        SolutionFile.file_id == FileMetadata.id,
    ).order_by(
        Solution.attached_task_id,
        User.id
    ).all()
    results: tp.List[str] = []
    header_row = f"user_id;" \
                 f"user_email;" \
                 f"lastname;" \
                 f"firstname;" \
                 f"patronym;" \
                 f"sol_id;" \
                 f"sol_date;" \
                 f"sol_user_id;" \
                 f"sol_attached_task_id;" \
                 f"sol_status;" \
                 f"sol_mark_auto;" \
                 f"sol_mark_final;" \
                 f"filename;" \
                 f"server_path;" \
                 f"hashsum;" \
                 f"url_to_file"
    for res in query_res:
        # ('/solution_file/<int:solution_id>/<description>')
        url = url_for('solution_file', solution_id=res[5], description=res[12])
        url = f'https://{APP_SETTINGS.APP_HOST}{url}'
        results.append(
            f'{res[0]};'
            f'{res[1]};'
            f'{res[2]};'
            f'{res[3]};'
            f'{res[4]};'
            f'{res[5]};'
            f'{res[6]};'
            f'{res[7]};'
            f'{res[8]};'
            f'{res[9]};'
            f'{res[10]};'
            f'{res[11]};'
            f'{res[12]};'
            f'{res[13]};'
            f'{res[14]};'
            f'{url}\n'
        )

    data = dict(results=results, header=header_row)
    template = Template(open('templates/csv_gen/generate_solutions.csv').read())
    csv_content = template.render(data)
    return send_file(BytesIO(csv_content.encode('utf-8')),
                     attachment_filename=f'generated_solutions_for_course_{course_id}.csv')


@app.route('/course/<int:course_id>/solutions_json')
@auth_user
def solutions_json(course_id):
    active_course = current_user.get_course_or_404(course_id)
    data = []
    for solution in active_course.solutions:
        d = {
            'id': solution.id,
            'date': solution.date.strftime('%d.%m.%Y %H:%M'),
            'user_fistname': solution.user.firstname,
            'user_lastname': solution.user.lastname,
            'info': solution.info,
            'task_name': solution.attached_task.task.name
        }
        if solution.mark_final is not None:
            d['mark'] = solution.mark_final
        data.append(d)

    return jsonify(data)


@app.route('/add_image', methods=['GET', 'POST'])
@auth_user
@templated()
def add_image():
    if not current_user.is_admin:
        abort(403)
    form = ImageForm()
    if form.validate_on_submit():
        img = Image(status='Waiting in queue', creator=current_user)
        text = form.build_script.data
        form.build_script.data = '\n'.join(text.splitlines())
        form.populate_obj(img)
        img.save()
        StrictRedis().lpush('images_inbox', img.id)
        flash('Образ успешно добавлен', 'success')
        return redirect(url_for('images'))
    return dict(form=form, endpoint='add_image')


@app.route('/edit_image/<int:image_id>', methods=['GET', 'POST'])
@auth_user
@templated('add_image.html')
def edit_image(image_id):
    if not current_user.is_admin:
        abort(403)
    img = Image.query.get_or_404(image_id)
    form = ImageForm(obj=img)
    if form.validate_on_submit():
        text = form.build_script.data
        form.build_script.data = '\n'.join(text.splitlines())
        form.populate_obj(img)
        img.status = 'Waiting in queue'
        img.build_log = ''
        img.save()
        StrictRedis().lpush('images_inbox', img.id)
        flash('Образ успешно обновлен', 'success')
        return redirect(url_for('images'))
    return dict(form=form, endpoint='edit_image')


@app.route('/images')
@auth_user
@templated()
def images():
    if not current_user.is_admin:
        abort(403)
    imgs = Image.query.filter(Image.creator == current_user) \
        .order_by(Image.id.desc()).all()
    return dict(imgs=imgs, endpoint='images')


@app.route('/task_file/<int:task_id>/<description>')
@auth_user
def task_file(task_id, description):
    task = Task.query.get_or_404(task_id)
    if not (current_user.is_task_availiable(task_id) or current_user.is_admin):
        abort(403)
    if description == 'private_tests' and not current_user.is_admin:
        abort(403)
    if description not in task.files:
        abort(404)
    return send_file(task.files[description].path, as_attachment=True,
                     attachment_filename=task.files[description].filename)


def open_notebook(path: str) -> tp.Any:
    from nbformat import read as nbread
    from nbconvert import HTMLExporter

    try:
        nb = nbread(open(path), as_version=4)
        html_data, _ = HTMLExporter().from_notebook_node(nb)
    except Exception as e:
        html_data = f'<h2>Данный ноутбук невозможно открыть :(</h2>'
    return html_data


@app.route('/course/<int:course_id>/task/<int:task_id>/review_solution_file/<int:peer_review_id>/<description>')
@auth_user
def review_solution_file(course_id, task_id, peer_review_id, description):
    active_course = current_user.get_course_or_404(course_id)
    if not (1 <= task_id <= len(active_course.tasks)):
        abort(404)
    active_task = active_course.tasks[task_id - 1]

    if not (
            active_task.use_peer_review and (
            active_task.finish_date < datetime.now() <= active_task.peer_review_date or current_user.is_admin)
    ):
        abort(404)

    solution_reviews = SolutionReview.query \
        .filter_by(rater=current_user) \
        .join(Solution) \
        .filter(Solution.attached_task == active_task) \
        .order_by(SolutionReview.id.asc()).all()

    if not (1 <= peer_review_id <= len(solution_reviews)):
        abort(404)

    solution_review = solution_reviews[peer_review_id - 1]
    solution = solution_review.solution
    if description not in solution.files:
        abort(404)
    if description.endswith('.ipynb'):
        html_data = open_notebook(solution.files[description].path)
        return send_file(BytesIO(html_data.encode('utf-8')),
                         attachment_filename=description.replace('.ipynb', '.html'))

    return send_file(solution.files[description].path,
                     attachment_filename=solution.files[description].filename)


@app.route('/solution_file/<int:solution_id>/<description>')
@auth_user
def solution_file(solution_id, description):
    solution = Solution.query.get_or_404(solution_id)
    if current_user.id != solution.user_id and not current_user.is_admin:
        abort(403)
    if description not in solution.files:
        abort(404)
    if description.endswith('.ipynb'):
        html_data = open_notebook(solution.files[description].path)
        return send_file(BytesIO(html_data.encode('utf-8')),
                         attachment_filename=description.replace('.ipynb', '.html'))

    return send_file(solution.files[description].path,
                     attachment_filename=solution.files[description].filename)


@app.route('/image_build_script/<int:image_id>')
@auth_user
def image_build_script(image_id):
    img = Image.query.get_or_404(image_id)
    return send_file(BytesIO(img.build_script.encode('utf-8')),
                     as_attachment=True, attachment_filename='build.sh')


@app.route('/static/<save_dir>/<filename>')
def serve_static(save_dir, filename):
    if save_dir in ['js', 'css', 'fonts']:
        return send_from_directory(join('static', save_dir), filename)
    abort(404)


def init_redis():
    StrictRedis().delete('solutions_inbox', 'images_inbox',
                         'solutions_processing', 'images_processing')


if __name__ == '__main__':
    if len(argv) == 2 and argv[1] == 'clean':
        init_db()
        init_redis()
        rmtree(UPLOAD_DIR, ignore_errors=True)
    else:
        app.run(debug=True)
