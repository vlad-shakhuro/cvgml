"""
Здесь нужно хранить все различия между версиями сайтов. Например, fall.cv-gml.ru и cv-gml.ru
Предпочтительнее использовать словарь, где ключ - это адрес сайта, а значение - может быть любым
"""
from wtforms.validators import Email, Regexp

# class RegisterForm(FlaskForm):
RESULT_REGEXP_REGISTER_FORM = {
    'fall.cv-gml.ru': [
        Email('адрес некорректен'),
    ],
    'cv-gml.ru': [
        Email('адрес некорректен'),
        Regexp(  # email validation regexp  s***@gse.cs.msu.ru
            '.*(msu.ru)',
            message=' email должен быть в домене msu.ru',
        ),
    ],
    'http://127.0.0.1': [
        Email('адрес некорректен'),
    ],
}
